﻿namespace ScratchEngine.Rendering
{
    /// <summary>
    /// 
    /// </summary>
    public struct ScanLineData
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public int currentY { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float ndotla { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float ndotlb { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float ndotlc { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float ndotld { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float ua { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float ub { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float uc { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float ud { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float va { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float vb { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float vc { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public float vd { get; set; }

        #endregion
    }
}