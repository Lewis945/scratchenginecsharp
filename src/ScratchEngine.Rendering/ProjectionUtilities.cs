﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math;

namespace ScratchEngine.Rendering
{
    /// <summary>
    /// 
    /// </summary>
    public static class ProjectionUtilities
    {
        /// <summary>
        /// Returns <see cref="Matrix4"/> orthographic projection matrix.
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="zNear"></param>
        /// <param name="zFar"></param>
        /// <returns>New <see cref="Matrix4"/> matrix.</returns>
        public static Matrix4 GetOrthographicMatrixLH(float width, float height, float zNear, float zFar)
        {
            float zRange = zFar - zNear;

            return new Matrix4(new float[,]
            {
                { 2 / width,     0,                 0,          0 },
                { 0,         2 / height,            0,          0 },
                { 0,             0,            1 / zRange,      0 },
                { 0,             0,       -zNear / zRange,      1 }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="zNear"></param>
        /// <param name="zFar"></param>
        /// <returns></returns>
        public static Matrix4 GetOrthographicMatrixRH(float width, float height, float zNear, float zFar)
        {
            float zRange = zFar - zNear;

            return new Matrix4(new float[,]
            {
                { 2 / width,     0,                 0,          0 },
                { 0,         2 / height,            0,          0 },
                { 0,             0,           1 / -zRange,      0 },
                { 0,             0,       zNear / -zRange,      1 }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="zNear"></param>
        /// <param name="zFar"></param>
        /// <returns></returns>
        public static Matrix4 GetOrthographicMatrixOffCenterLH(float width, float height, float zNear, float zFar)
        {
            float l = -width / 2;
            float r = width / 2;
            float b = -height / 2;
            float t = height / 2;
            float zRange = zFar - zNear;

            return new Matrix4(new float[,]
            {
                {   2/(r-l),             0,              0,          0 },
                {      0,             2/(t-b),           0,          0 },
                {      0,                0,           1/zRange,      0 },
                { (l+r)/(l-r),      (t+b)/(b-t),    zNear/-zRange,   1 }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="width"></param>
        /// <param name="height"></param>
        /// <param name="zNear"></param>
        /// <param name="zFar"></param>
        /// <returns></returns>
        public static Matrix4 GetOrthographicMatrixOffCenterRH(float width, float height, float zNear, float zFar)
        {
            float l = -width / 2;
            float r = width / 2;
            float b = -height / 2;
            float t = height / 2;
            float zRange = zFar - zNear;

            return new Matrix4(new float[,]
            {
                {   2/(r-l),             0,               0,          0 },
                {      0,             2/(t-b),            0,          0 },
                {      0,                0,           1/-zRange,      0 },
                { (l+r)/(l-r),      (t+b)/(b-t),    zNear/-zRange,    1 }
            });
        }

        /// <summary>
        /// Returns <see cref="Matrix4"/> perspective projection matrix.
        /// </summary>
        /// <param name="fov">fovy radians</param>
        /// <param name="aspectRatio"></param>
        /// <param name="zNear"></param>
        /// <param name="zFar"></param>
        /// <returns>New <see cref="Matrix4"/> matrix.</returns>
        public static Matrix4 GetPerspectiveMatrixLH(float fov, float aspectRatio, float zNear, float zFar)
        {
            float tanHalfFov = (float)System.Math.Tan(fov / 2);
            float zRange = zFar - zNear;
            float yScale = 1 / tanHalfFov;
            float xScale = yScale / aspectRatio;

            return new Matrix4(new float[,]
            {
                { xScale,        0,                         0,     0 },
                {      0,   yScale,                         0,     0 },
                {      0,        0,             zFar / zRange,     1 },
                {      0,        0,    -zNear * zFar / zRange,     0 }
            });
        }

        /// <summary>
        /// Returns <see cref="Matrix4"/> perspective projection matrix.
        /// </summary>
        /// <param name="fov">fovy radians</param>
        /// <param name="aspectRatio"></param>
        /// <param name="zNear"></param>
        /// <param name="zFar"></param>
        /// <returns>New <see cref="Matrix4"/> matrix.</returns>
        public static Matrix4 GetPerspectiveMatrixRH(float fov, float aspectRatio, float zNear, float zFar)
        {
            float tanHalfFov = (float)System.Math.Tan(fov / 2);
            float zRange = zFar - zNear;
            float yScale = 1 / tanHalfFov;
            float xScale = yScale / aspectRatio;

            return new Matrix4(new float[,]
            {
                { xScale,        0,                         0,     0 },
                {      0,   yScale,                         0,     0 },
                {      0,        0,            zFar / -zRange,    -1 },
                {      0,        0,    zNear * zFar / -zRange,     0 }
            });
        }
    }
}