﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math;

namespace ScratchEngine.Rendering
{
    /// <summary>
    /// 
    /// </summary>
    public class Mesh
    {
        #region Fields

        #endregion

        #region Properties

        public string Name { get; private set; }

        public Vertex[] Vertices { get; private set; }

        public Face[] Faces { get; set; }

        public Vector3 Position { get; set; }
        public Vector3 Rotation { get; set; }
        public Vector3 Scale { get; set; }

        public Texture Texture { get; set; }

        #endregion

        #region .ctor

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="verticesCount"></param>
        public Mesh(string name, int verticesCount, int facesCount)
        {
            Vertices = new Vertex[verticesCount];
            Faces = new Face[facesCount];
            Scale = new Vector3(1, 1, 1);
            Name = name;
        }

        #endregion
    }
}