﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math;

namespace ScratchEngine.Rendering
{
    /// <summary>
    /// 
    /// </summary>
    public static class ViewUtilities
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="forwardDirection"></param>
        /// <param name="upDirection"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        public static Matrix4 GetViewMatrixRH(Vector3 forwardDirection, Vector3 upDirection, Vector3 position)
        {
            var zaxis = -forwardDirection;  // The "forward" vector.

            var xaxis = upDirection.CrossProduct(zaxis);   // The "right" vector.
            xaxis.Normalize();
            // or
            // var xaxis = RightDirection;

            var yaxis = zaxis.CrossProduct(xaxis);     // The "up" vector.

            // Create a 4x4 orientation matrix from the right, up, and forward vectors
            var viewMatrix = new Matrix4(new float[,]{
               { xaxis.X,                       yaxis.X,                        zaxis.X,                       0 },
               { xaxis.Y,                       yaxis.Y,                        zaxis.Y,                       0 },
               { xaxis.Z,                       yaxis.Z,                        zaxis.Z,                       0 },
               { xaxis.DotProduct(position),    yaxis.DotProduct(position),     zaxis.DotProduct(position),    1 }
            });
            return viewMatrix;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="forwardDirection"></param>
        /// <param name="upDirection"></param>
        /// <param name="position"></param>
        /// <returns></returns>
        public static Matrix4 GetViewMatrixLH(Vector3 forwardDirection, Vector3 upDirection, Vector3 position)
        {
            var zaxis = forwardDirection;  // The "forward" vector.

            var xaxis = upDirection.CrossProduct(zaxis);   // The "right" vector.
            xaxis.Normalize();
            // or
            // var xaxis = RightDirection;

            var yaxis = zaxis.CrossProduct(xaxis);     // The "up" vector.

            var viewMatrix = new Matrix4(new float[,]{
               { xaxis.X,                       yaxis.X,                        zaxis.X,                        0 },
               { xaxis.Y,                       yaxis.Y,                        zaxis.Y,                        0 },
               { xaxis.Z,                       yaxis.Z,                        zaxis.Z,                        0 },
               { -xaxis.DotProduct(position),   -yaxis.DotProduct(position),    -zaxis.DotProduct(position),    1 }
            });
            return viewMatrix;
        }
    }
}
