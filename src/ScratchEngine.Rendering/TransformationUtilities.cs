﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math;
using ScratchEngine.Math.Exceptions;

namespace ScratchEngine.Rendering
{
    // Right handed (Z front direction): X x Y = Z
    // Left handed   (Z back direction): X x Y = -Z

    /// <summary>
    /// 
    /// </summary>
    public static class TransformationUtilities
    {
        #region Translation

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dx"></param>
        /// <param name="dy"></param>
        /// <param name="dz"></param>
        /// <returns></returns>
        public static Matrix4 GetTranslationMatrix(float dx, float dy, float dz)
        {
            var matrix = Matrix4.Identity;

            //              ( 1  0  0  0 )
            //T(dx,dy,dz) = ( 0  0  0  0 )
            //              ( 0  0  0  0 )
            //              ( dx dy dz 1 )

            matrix[matrix.Length - 1, 0] = dx;
            matrix[matrix.Length - 1, 1] = dy;
            matrix[matrix.Length - 1, 2] = dz;

            return matrix;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Matrix4 GetTranslationMatrix(Vector3 v)
        {
            return GetTranslationMatrix(v.X, v.Y, v.Z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="delta"></param>
        /// <param name="axle"></param>
        /// <returns></returns>
        public static Matrix4 GetTranslationMatrix(float delta, Axle axle)
        {
            float dx = 0;
            float dy = 0;
            float dz = 0;

            if (axle == Axle.X) dx = delta;
            else if (axle == Axle.Y) dy = delta;
            else if (axle == Axle.Z) dz = delta;

            return GetTranslationMatrix(dx, dy, dz);
        }

        #endregion

        #region Rotations

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle">radians</param>
        /// <param name="axle"></param>
        /// <returns></returns>
        public static Matrix4 GetRotationMatrix(float angle, Axle axle)
        {
            var matrix = Matrix4.Identity;

            if (axle == Axle.X)
            {
                //        ( 1   0       0     0 )
                //Mx(a) = ( 0  cos(a)  sin(a) 0 )
                //        ( 0 -sin(a)  cos(a) 0 )
                //        ( 0   0       0     1 )

                matrix[1, 1] = (float)System.Math.Cos(angle); matrix[1, 2] = (float)System.Math.Sin(angle);
                matrix[2, 1] = -(float)System.Math.Sin(angle); matrix[2, 2] = (float)System.Math.Cos(angle);
            }
            else if (axle == Axle.Y)
            {
                //        ( cos(a) 0 -sin(a) 0 )
                //My(a) = (    0   1    0    0 )
                //        ( sin(a) 0  cos(a) 0 )
                //        (    0   0    0    1 )

                matrix[0, 0] = (float)System.Math.Cos(angle); matrix[0, 2] = -(float)System.Math.Sin(angle);
                matrix[2, 0] = (float)System.Math.Sin(angle); matrix[2, 2] = (float)System.Math.Cos(angle);
            }
            else if (axle == Axle.Z)
            {
                //        (  cos(a) sin(a) 0 0 )
                //Mz(a) = ( -sin(a) cos(a) 0 0 )
                //        (    0      0    0 0 )
                //        (    0      0    0 1 )

                matrix[0, 0] = (float)System.Math.Cos(angle); matrix[0, 1] = (float)System.Math.Sin(angle);
                matrix[1, 0] = -(float)System.Math.Sin(angle); matrix[1, 1] = (float)System.Math.Cos(angle);
            }

            return matrix;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angleX"></param>
        /// <param name="angleY"></param>
        /// <param name="angleZ"></param>
        /// <returns></returns>
        public static Matrix4 GetRotationMatrix(float angleX, float angleY, float angleZ)
        {
            var mx = GetRotationMatrix(angleX, Axle.X);
            var my = GetRotationMatrix(angleY, Axle.Y);
            var mz = GetRotationMatrix(angleZ, Axle.Z);

            return mx * my * mz;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Matrix4 GetRotationMatrix(Vector3 v)
        {
            return GetRotationMatrix(v.X, v.Y, v.Z);
        }

        #endregion

        #region Scale

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dx"></param>
        /// <param name="dy"></param>
        /// <param name="dz"></param>
        /// <returns></returns>
        public static Matrix4 GetScaleMatrix(float dx, float dy, float dz)
        {
            if (dx == 0 || dy == 0 || dz == 0)
                throw new MathException(string.Format("Can't scale by 0. dx={0}, dy={1}, dz={2}.", dx, dy, dz));

            var matrix = Matrix4.Identity;

            //              ( dx 0  0  0 )
            //S(dx,dy,dz) = ( 0  dy 0  0 )
            //              ( 0  0  dz 0 )
            //              ( 0  0  0  1 )

            matrix[0, 0] = dx;
            matrix[1, 1] = dy;
            matrix[2, 2] = dz;

            return matrix;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Matrix4 GetScaleMatrix(Vector3 v)
        {
            return GetScaleMatrix(v.X, v.Y, v.Z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="delta"></param>
        /// <param name="axle"></param>
        /// <returns></returns>
        public static Matrix4 GetScaleMatrix(float delta, Axle axle)
        {
            float dx = 1;
            float dy = 1;
            float dz = 1;

            if (axle == Axle.X) dx = delta;
            else if (axle == Axle.Y) dy = delta;
            else if (axle == Axle.Z) dz = delta;

            return GetScaleMatrix(dx, dy, dz);
        }

        #endregion
    }
}