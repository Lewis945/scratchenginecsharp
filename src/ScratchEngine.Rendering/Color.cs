﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

namespace ScratchEngine.Rendering
{
    /// <summary>
    /// 
    /// </summary>
    public struct Color
    {
        #region Defaults

        /// <summary>
        /// Returns black color <see cref="Color"/> instance.
        /// </summary>
        public static Color Black { get; } = new Color(0, 0, 0);

        /// <summary>
        /// Returns white color <see cref="Color"/> instance.
        /// </summary>
        public static Color White { get; } = new Color(1, 1, 1);

        /// <summary>
        /// Returns red color <see cref="Color"/> instance.
        /// </summary>
        public static Color Red { get; } = new Color(1, 0, 0);

        /// <summary>
        /// Returns green color <see cref="Color"/> instance.
        /// </summary>
        public static Color Green { get; } = new Color(0, 1, 0);

        /// <summary>
        /// Returns blue color <see cref="Color"/> instance.
        /// </summary>
        public static Color Blue { get; } = new Color(0, 0, 1);

        #endregion

        #region Fields

        private float _r;
        private float _g;
        private float _b;
        private float _a;

        #endregion

        #region Properties

        /// <summary>
        /// Red <see cref="float"/> value in [0,1] range.
        /// </summary>
        public float R { get { return _r; } }

        /// <summary>
        /// Green <see cref="float"/> value in [0,1] range.
        /// </summary>
        public float G { get { return _g; } }

        /// <summary>
        /// Blue <see cref="float"/> value in [0,1] range.
        /// </summary>
        public float B { get { return _b; } }

        /// <summary>
        /// Alpha <see cref="float"/> value in [0,1] range.
        /// </summary>
        public float A { get { return _a; } }

        #endregion

        #region .ctor

        /// <summary>
        /// Instantiates <see cref="Color"/> struct with (r,g,b) components.
        /// </summary>
        /// <param name="r">Red <see cref="float"/> value in [0,1] range.</param>
        /// <param name="g">Green <see cref="float"/> value in [0,1] range.</param>
        /// <param name="b">Blue <see cref="float"/> value in [0,1] range.</param>
        public Color(float r, float g, float b)
        {
            _r = r;
            _g = g;
            _b = b;
            _a = 1;
        }

        /// <summary>
        /// Instantiates <see cref="Color"/> struct with (r,g,b,a) components.
        /// </summary>
        /// <param name="r">Red <see cref="float"/> value in [0,1] range.</param>
        /// <param name="g">Green <see cref="float"/> value in [0,1] range.</param>
        /// <param name="b">Blue <see cref="float"/> value in [0,1] range.</param>
        /// <param name="a">Alpha <see cref="float"/> value in [0,1] range.</param>
        public Color(float r, float g, float b, float a)
            : this(r, g, b)
        {
            _a = a;
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Multiplies <see cref="Color"/> c with <see cref="float"/> scalar s.
        /// </summary>
        /// <param name="c">Color to multiply with.</param>
        /// <param name="s">Scalar to multiply.</param>
        /// <returns>New <see cref="Color"/> color.</returns>
        public static Color Multiply(Color c, float s)
        {
            Color result;

            result._r = c._r * s;
            result._g = c._g * s;
            result._b = c._b * s;
            result._a = c._a * s;

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
        public static Color Multiply(Color c1, Color c2)
        {
            Color result;

            result._r = c1._r * c2._r;
            result._g = c1._g * c2._g;
            result._b = c1._b * c2._b;
            result._a = c1._a * c2._a;

            return result;
        }

        /// <summary>
        /// Adds <see cref="Color"/> c2 to <see cref="Color"/> c1.
        /// </summary>
        /// <param name="c1">Color to add to.</param>
        /// <param name="c2">Color to add.</param>
        /// <returns>New <see cref="Color"/> color.</returns>
        public static Color Add(Color c1, Color c2)
        {
            Color result;

            result._r = c1._r + c2._r;
            result._g = c1._g + c2._g;
            result._b = c1._b + c2._b;
            result._a = c1._a + c2._a;

            return result;
        }

        /// <summary>
        /// Compares colors (<see cref="Color"/>) for equality.
        /// </summary>
        /// <param name="c1">Color to compare.</param>
        /// <param name="c2">Color comapre to.</param>
        /// <returns>Boolean result of a check.</returns>
        public static bool Equals(Color c1, Color c2)
        {
            return c1.Equals(c2);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Multiplies this <see cref="Color"/> object with <see cref="float"/> scalar s.
        /// </summary>
        /// <param name="s">Scalar to multiply.</param>
        /// <returns>New <see cref="Color"/> color.</returns>
        public Color Multiply(float s)
        {
            return Multiply(this, s);
        }

        /// <summary>
        /// Adds <see cref="Color"/> c to this <see cref="Color"/> object.
        /// </summary>
        /// <param name="c">Color to add.</param>
        /// <returns>New <see cref="Color"/> color.</returns>
        public Color Add(Color c)
        {
            return Add(this, c);
        }

        /// <summary>
        /// Checks if this <see cref="Color"/> object is equal to <see cref="Color"/> c.
        /// </summary>
        /// <param name="c">Color compare to.</param>
        /// <returns>Boolean result of a check.</returns>
        public bool Equals(Color c)
        {
            if (c == null)
                return false;

            return (_r == c._r) && (_g == c._g) && (_b == c._b) && (_a == c._a);
        }

        #endregion

        #region Operators

        /// <summary>
        /// Multiplies <see cref="Color"/> c with <see cref="float"/> scalar s.
        /// </summary>
        /// <param name="c">Color to multiply with.</param>
        /// <param name="s">Scalar to multiply.</param>
        /// <returns>New <see cref="Color"/> color.</returns>
        public static Color operator *(Color c, float s)
        {
            return Multiply(c, s);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="c1"></param>
        /// <param name="c2"></param>
        /// <returns></returns>
        public static Color operator *(Color c1, Color c2)
        {
            return Multiply(c1, c2);
        }

        /// <summary>
        /// Adds <see cref="Color"/> c2 to <see cref="Color"/> c1.
        /// </summary>
        /// <param name="c1">Color to add to.</param>
        /// <param name="c2">Color to add.</param>
        /// <returns>New <see cref="Color"/> color.</returns>
        public static Color operator +(Color c1, Color c2)
        {
            return Add(c1, c2);
        }

        /// <summary>
        /// Compares colors (<see cref="Color"/>) for equality.
        /// </summary>
        /// <param name="c1">Color to compare.</param>
        /// <param name="c2">Color comapre to.</param>
        /// <returns>Boolean result of a check.</returns>
        public static bool operator ==(Color c1, Color c2)
        {
            return c1.Equals(c2);
        }

        /// <summary>
        /// Compares colors (<see cref="Color"/>) for inequality.
        /// </summary>
        /// <param name="c1">Color to compare.</param>
        /// <param name="c2">Color comapre to.</param>
        /// <returns>Boolean result of a check.</returns>
        public static bool operator !=(Color c1, Color c2)
        {
            return !c1.Equals(c2);
        }

        #endregion

        #region Override

        /// <summary>
        /// Checks if this <see cref="Color"/> object is equal to <see cref="object"/> obj.
        /// </summary>
        /// <param name="obj">Object compare to.</param>
        /// <returns>Boolean result of a check.</returns>
        public override bool Equals(object obj)
        {
            // If parameter cannot be cast to Color return false.
            if (!(obj is Color))
                return false;

            // Return true if the fields match:
            return Equals((Color)obj);
        }

        /// <summary>
        /// Computes hashcode based on (r,g,b,a) components for <see cref="Color"/>.
        /// </summary>
        /// <returns>Hashcode.</returns>
        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + _r.GetHashCode();
            hash = (hash * 7) + _g.GetHashCode();
            hash = (hash * 7) + _b.GetHashCode();
            hash = (hash * 7) + _a.GetHashCode();
            return hash;
        }

        /// <summary>
        /// Prints <see cref="Color"/> as "Color(r = {r}, g = {g}, b = {b}, a = {a})".
        /// </summary>
        /// <returns>String representation of the <see cref="Color"/>.</returns>
        public override string ToString()
        {
            return string.Format("Color(r = {0}, g = {1}, b = {2}, a = {3})", _r, _g, _b, _a);
        }

        #endregion
    }
}