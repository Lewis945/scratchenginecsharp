﻿using ScratchEngine.Math;

namespace ScratchEngine.Rendering
{
    /// <summary>
    /// 
    /// </summary>
    public struct Vertex
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Vector3 Normal { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Vector3 Coordinates { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Vector3 WorldCoordinates { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Vector2 TextureCoordinates { get; set; }

        #endregion
    }
}