﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math;

namespace ScratchEngine.Rendering
{
    /// <summary>
    /// 
    /// </summary>
    public struct Face
    {
        #region Fields

        private int _a;
        private int _b;
        private int _c;

        private Vector3 _normal;

        #endregion

        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public int A { get { return _a; } }

        /// <summary>
        /// 
        /// </summary>
        public int B { get { return _b; } }

        /// <summary>
        /// 
        /// </summary>
        public int C { get { return _c; } }

        /// <summary>
        /// 
        /// </summary>
        public Vector3 Normal { get { return _normal; } }

        #endregion

        #region .ctor

        public Face(int a, int b, int c, Vector3 normal)
        {
            _a = a;
            _b = b;
            _c = c;
            _normal = normal;
        }

        #endregion
    }
}