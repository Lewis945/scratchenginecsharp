﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math;

namespace ScratchEngine.Rendering
{
    /// <summary>
    /// 
    /// </summary>
    public class Camera
    {
        #region Defaults

        #endregion

        #region Fields

        private float _zNear;
        private float _zFar;

        private Vector3 _position;
        private Vector3 _forwardDirection;
        private Vector3 _upDirection;

        #endregion

        #region Properties

        /// <summary>
        /// z coordinate of the near plane clipping plane.
        /// </summary>
        public float ZNear { get { return _zNear; } }

        /// <summary>
        /// z coordinate of the far plane clipping plane.
        /// </summary>
        public float ZFar { get { return _zFar; } }

        /// <summary>
        /// 
        /// </summary>
        public Vector3 Position { get { return _position; } }

        /// <summary>
        /// 
        /// </summary>
        public Vector3 ForwardDirection { get { return _forwardDirection; } }

        /// <summary>
        /// 
        /// </summary>
        //public Vector3 BackDirection { get { return -_forwardDirection; } }

        /// <summary>
        /// 
        /// </summary>
        public Vector3 UpDirection { get { return _upDirection; } }

        /// <summary>
        /// 
        /// </summary>
        //public Vector3 DownDirection { get { return -_upDirection; } }

        /// <summary>
        /// 
        /// </summary>
        public Vector3 RightDirection { get { return _upDirection.CrossProduct(_forwardDirection); } }

        /// <summary>
        /// 
        /// </summary>
        public Vector3 LeftDirection { get { return _forwardDirection.CrossProduct(_upDirection); } }

        #endregion

        #region .ctor

        /// <summary>
        /// 
        /// </summary>
        public Camera()
            : this(Vector3.Origin, Vector3.BasisZ, Vector3.BasisY)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="forwardDirection"></param>
        /// <param name="upDirection"></param>
        public Camera(Vector3 position, Vector3 forwardDirection, Vector3 upDirection)
        {
            _position = position;
            _forwardDirection = forwardDirection;
            _upDirection = upDirection;
        }

        #endregion

        #region Static Methods

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="position"></param>
        /// <param name="lookAt"></param>
        /// <param name="upDirection"></param>
        public void LookAt(Vector3 position, Vector3 lookAt, Vector3 upDirection)
        {
            _position = position;
            var distance = lookAt - position;
            _forwardDirection = distance.Normalize();
            _upDirection = upDirection.Normalize();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="amount"></param>
        public void Move(Vector3 direction, float amount)
        {
            _position = _position + direction * amount;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        public void RotateX(float angle)
        {
            var horizontalAxis = Vector3.BasisY.CrossProduct(_forwardDirection);
            horizontalAxis = horizontalAxis.Normalize();

            _forwardDirection = _forwardDirection.Rotate(angle, horizontalAxis);
            _forwardDirection = _forwardDirection.Normalize();

            _upDirection = _forwardDirection.CrossProduct(horizontalAxis);
            _upDirection = _upDirection.Normalize();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        public void RotateY(float angle)
        {
            var horizontalAxis = Vector3.BasisY.CrossProduct(_forwardDirection);
            horizontalAxis = horizontalAxis.Normalize();

            _forwardDirection = _forwardDirection.Rotate(angle, Vector3.BasisY);
            _forwardDirection = _forwardDirection.Normalize();

            _upDirection = _forwardDirection.CrossProduct(horizontalAxis);
            _upDirection = _upDirection.Normalize();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Matrix4 GetViewMatrixRH()
        {
            return ViewUtilities.GetViewMatrixRH(_forwardDirection, _upDirection, _position); ;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Matrix4 GetViewMatrixLH()
        {
            return ViewUtilities.GetViewMatrixLH(_forwardDirection, _upDirection, _position);
        }

        #endregion

        #region Override

        #endregion
    }
}