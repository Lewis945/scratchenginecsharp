﻿namespace ScratchEngine.Rendering
{
    /// <summary>
    /// 
    /// </summary>
    public class Texture
    {
        private string _fileName;
        private byte[] _internalBuffer;
        private int _width;
        private int _height;

        public string FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        public byte[] InternalBuffer
        {
            get { return _internalBuffer; }
            set { _internalBuffer = value; }
        }

        public int Width { get { return _width; } }
        public int Height { get { return _height; } }

        // Working with a fix sized texture (512x512, 1024x1024, etc.).
        public Texture(string fileName, int width, int height)
        {
            _width = width;
            _height = height;
            _fileName = fileName;
        }

        // Takes the U & V coordinates exported by Blender
        // and return the corresponding pixel color in the texture
        public Color Map(float tu, float tv)
        {
            // Image is not loaded yet
            if (_internalBuffer == null)
            {
                return Color.White;
            }
            // using a % operator to cycle/repeat the texture if needed
            int u = System.Math.Abs((int)(tu * _width) % _width);
            int v = System.Math.Abs((int)(tv * _height) % _height);

            int pos = (u + v * _width) * 4;
            byte b = _internalBuffer[pos];
            byte g = _internalBuffer[pos + 1];
            byte r = _internalBuffer[pos + 2];
            byte a = _internalBuffer[pos + 3];

            return new Color(r / 255.0f, g / 255.0f, b / 255.0f, a / 255.0f);
        }
    }
}