﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math.Interfaces;
using System;

namespace ScratchEngine.Math
{
    /// <summary>
    /// 
    /// </summary>
    public struct Quaternion : IEquatable<Quaternion>, IMultiplication<Quaternion>
    {
        #region Defaults

        /// <summary>
        /// 
        /// </summary>
        public static Quaternion Default { get { return new Quaternion(1, 0, 0, 0); } }

        #endregion

        #region Fields

        private readonly float _w;
        private readonly float _x;
        private readonly float _y;
        private readonly float _z;

        #endregion

        #region Properties

        /// <summary>
        /// w component.
        /// </summary>
        public float W { get { return _w; } }

        /// <summary>
        /// x coordinate.
        /// </summary>
        public float X { get { return _x; } }

        /// <summary>
        /// y coordinate.
        /// </summary>
        public float Y { get { return _y; } }

        /// <summary>
        /// z coordinate.
        /// </summary>
        public float Z { get { return _z; } }

        /// <summary>
        /// Length (magnitude, norm) of the vector.
        /// </summary>
        public int Length { get { return GetLength(this); } }

        #endregion

        #region .ctor

        /// <summary>
        /// Instantiates <see cref="Vector3"/> struct with (x,y,z) coordinates.
        /// </summary>
        /// <param name="w">w component.</param>
        /// <param name="x">x coordinate.</param>
        /// <param name="y">y coordinate.</param>
        /// <param name="z">z coordinate.</param>
        public Quaternion(float w, float x, float y, float z)
        {
            _w = w;
            _x = x;
            _y = y;
            _z = z;
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle"></param>
        /// <param name="axis"></param>
        /// <returns></returns>
        public static Quaternion GetLocalRotationQuaternion(float angle, Vector3 axis)
        {
            float sinHalhAngle = (float)System.Math.Sin((angle / 2).ToRadians());
            float cosHalhAngle = (float)System.Math.Cos((angle / 2).ToRadians());

            float w = cosHalhAngle;
            float x = axis.X * sinHalhAngle;
            float y = axis.Y * sinHalhAngle;
            float z = axis.Z * sinHalhAngle;

            return new Quaternion(w, x, y, z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public static int GetLength(Quaternion q)
        {
            return (int)System.Math.Sqrt(q.W * q.W + q.X * q.X + q.Y * q.Y + q.Z * q.Z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Quaternion Normilize(Quaternion q)
        {
            int length = GetLength(q);
            return new Quaternion(q.W / length, q.X / length, q.Y / length, q.Z / length);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Quaternion Conjugate(Quaternion q)
        {
            return new Quaternion(q._w, -q._x, -q._y, -q._z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Matrix4 GetRotationMatrix(Quaternion q)
        {
            return new Matrix4(new float[,] {
                { (q._w * q._w + q._x * q._x - q._y * q._y - q._z * q._z), (2 * q._x * q._y - 2 * q._w * q._z), (2 * q._x * q._z + 2 * q._w * q._y), 0 },
                { (2 * q._x * q._y + 2 * q._w * q._z), (q._w * q._w - q._x * q._x + q._y * q._y - q._z * q._z), (2 * q._y * q._z + 2 * q._w * q._z), 0 },
                { (2 * q._x * q._z - 2 * q._w * q._y), (2 * q._y * q._z - 2 * q._w * q._z), (q._w * q._w - q._x * q._x - q._y * q._y + q._z * q._z), 0 },
                { 0, 0, 0, 1 }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Matrix4 GetRotationMatrixWithNormalization(Quaternion q)
        {
            q = q.Normilize();

            return new Matrix4(new float[,] {
                { (1 - 2 * q._y * q._y - 2 * q._z * q._z), (2 * q._x * q._y - 2 * q._w * q._z), (2 * q._x * q._z + 2 * q._w * q._y), 0 },
                { (2 * q._x * q._y + 2 * q._w * q._z), (1 - 2 * q._x * q._x - 2 * q._z * q._z), (2 * q._y * q._z + 2 * q._w * q._z), 0 },
                { (2 * q._x * q._z - 2 * q._w * q._y), (2 * q._y * q._z - 2 * q._w * q._z), (1 - 2 * q._x * q._x - 2 * q._y * q._y), 0 },
                { 0, 0, 0, 1 }
            });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="q1"></param>
        /// <param name="q2"></param>
        /// <returns></returns>
        public static Quaternion Multiply(Quaternion q1, Quaternion q2)
        {
            float w = q1.W * q2.W - q1.X * q2.X - q1.Y * q2.Y - q1.Z * q2.Z;
            float x = q1.W * q2.X + q1.X * q2.W + q1.Y * q2.Z - q1.Z * q2.Y;
            float y = q1.W * q2.Y - q1.X * q2.Z + q1.Y * q2.W + q1.Z * q2.X;
            float z = q1.W * q2.Z + q1.X * q2.Y - q1.Y * q2.X + q1.Z * q2.W;
            return new Quaternion(w, x, y, z);
        }

        /// <summary>
        /// Compares quaternions (<see cref="Quaternion"/>) for equality.
        /// </summary>
        /// <param name="q1">Quaternion to compare.</param>
        /// <param name="q2">Quaternion comapre to.</param>
        /// <returns>Boolean result of a check.</returns>
        public static bool Equals(Quaternion q1, Quaternion q2)
        {
            if (q1 == null || q2 == null)
                return false;

            return (q1._w == q2._w) && (q1._x == q2._x) && (q1._y == q2._y) && (q1._z == q2._z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public static int GetHashCode(Quaternion q)
        {
            int hash = 13;
            hash = (hash * 7) + q._w.GetHashCode();
            hash = (hash * 7) + q._x.GetHashCode();
            hash = (hash * 7) + q._y.GetHashCode();
            hash = (hash * 7) + q._z.GetHashCode();
            return hash;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public static Vector3 ToVector(Quaternion q)
        {
            return new Vector3(q._x, q._y, q._z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public static string ToString(Quaternion q)
        {
            return string.Format("Quaternion(w = {0}, x = {1}, y = {2}, z = {3})", q._w, q._x, q._y, q._z);
        }

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Quaternion Normilize()
        {
            return Normilize(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Quaternion Conjugate()
        {
            return Conjugate(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="q"></param>
        /// <returns></returns>
        public Quaternion Multiply(Quaternion q)
        {
            return Multiply(this, q);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Matrix4 GetRotationMatrix()
        {
            return GetRotationMatrix(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Matrix4 GetRotationMatrixWithNormalization()
        {
            return GetRotationMatrixWithNormalization(this);
        }

        /// <summary>
        /// Checks if this <see cref="Quaternion"/> object is equal to <see cref="Quaternion"/> q.
        /// </summary>
        /// <param name="q">Vector to compare to.</param>
        /// <returns>Boolean result of a check.</returns>
        public bool Equals(Quaternion q)
        {
            return Equals(this, q);
        }

        #endregion

        #region Operators

        /// <summary>
        /// 
        /// </summary>
        /// <param name="q1"></param>
        /// <param name="q2"></param>
        /// <returns></returns>
        public static Quaternion operator *(Quaternion q1, Quaternion q2)
        {
            return Multiply(q1, q2);
        }

        /// <summary>
        /// Checks if <see cref="Quaternion"/> q1 is equal to <see cref="Quaternion"/> q2.
        /// </summary>
        /// <param name="q1">Quaternion to compare.</param>
        /// <param name="q2">Quaternion compare to.</param>
        /// <returns>Boolean result of a check.</returns>
        public static bool operator ==(Quaternion q1, Quaternion q2)
        {
            return Equals(q1, q2);
        }

        /// <summary>
        /// Checks if <see cref="Quaternion"/> q1 is not equal to <see cref="Quaternion"/> q2.
        /// </summary>
        /// <param name="q1">Quaternion to compare.</param>
        /// <param name="q2">Quaternion compare to.</param>
        /// <returns>Boolean result of a check.</returns>
        public static bool operator !=(Quaternion q1, Quaternion q2)
        {
            return !Equals(q1, q2);
        }

        #endregion

        #region Override

        /// <summary>
        /// Checks if this <see cref="Quaternion"/> object is equal to <see cref="object"/> obj.
        /// </summary>
        /// <param name="obj">Object to compare to.</param>
        /// <returns>Boolean result of a check.</returns>
        public override bool Equals(object obj)
        {
            // If parameter cannot be cast to Vector3 return false.
            if (!(obj is Quaternion))
                return false;

            // Return true if the fields match:
            return Equals((Quaternion)obj);
        }

        /// <summary>
        /// Computes hashcode based on (w,x,y,z) components for <see cref="Quaternion"/>.
        /// </summary>
        /// <returns>Hashcode.</returns>
        public override int GetHashCode()
        {
            return GetHashCode(this);
        }

        /// <summary>
        /// Prints <see cref="Quaternion"/> as "Quaternion(w = {w}, x = {x}, y = {y}, z = {z})".
        /// </summary>
        /// <returns>String representation of the <see cref="Quaternion"/>.</returns>
        public override string ToString()
        {
            return ToString(this);
        }

        #endregion
    }
}