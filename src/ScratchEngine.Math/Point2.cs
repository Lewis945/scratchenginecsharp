﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math.Interfaces;
using System;

namespace ScratchEngine.Math
{
    /// <summary>
    /// Struct represents a point in 2D space (Carthesian).
    /// </summary>
    public struct Point2 : IEquatable<Point2>, IAddition<Point2>, ISubtraction<Vector2, Point2>
    {
        #region Defaults

        /// <summary>
        /// Origin <see cref="Point2"/> point.
        /// </summary>
        public static Point2 Origin { get { return new Point2(0, 0); } }

        #endregion

        #region Fields

        private readonly float _x;
        private readonly float _y;

        #endregion

        #region Properties

        /// <summary>
        /// x coordinate.
        /// </summary>
        public float X { get { return _x; } }

        /// <summary>
        /// y coordinate.
        /// </summary>
        public float Y { get { return _y; } }

        #endregion

        #region .ctor

        /// <summary>
        /// Instantiates <see cref="Point2"/> struct with (x,y) coordinates.
        /// </summary>
        /// <param name="x">x coordinate.</param>
        /// <param name="y">y coordinate.</param>
        public Point2(float x, float y)
        {
            _x = x;
            _y = y;
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Adds <see cref="Point2"/> p2 to <see cref="Point2"/> p1.
        /// </summary>
        /// <param name="p1">Point to add to.</param>
        /// <param name="p2">Point to add.</param>
        /// <returns>New point.</returns>
        public static Point2 Add(Point2 p1, Point2 p2)
        {
            return new Point2(p1.X + p2.X, p1.Y + p2.Y);
        }

        /// <summary>
        /// Adds <see cref="Vector2"/> v to <see cref="Point2"/> p.
        /// </summary>
        /// <param name="p">Point to add to.</param>
        /// <param name="v">Vector to add.</param>
        /// <returns>New point.</returns>
        public static Point2 Add(Point2 p, Vector2 v)
        {
            return new Point2(p.X + v.X, p.Y + v.Y);
        }

        /// <summary>
        /// Subtracts <see cref="Point2"/> p2 from <see cref="Point2"/> p1.
        /// </summary>
        /// <param name="p1">Point to substrat from.</param>
        /// <param name="p2">Point to subtract.</param>
        /// <returns>New vector.</returns>
        public static Vector2 Subtract(Point2 p1, Point2 p2)
        {
            return new Vector2(p1.X - p2.X, p1.Y - p2.Y);
        }

        /// <summary>
        /// Subtracts <see cref="Vector2"/> v from <see cref="Point2"/> p.
        /// </summary>
        /// <param name="p">Point to subtract from.</param>
        /// <param name="v">Vector to subtract.</param>
        /// <returns>New point.</returns>
        public static Point2 Subtract(Point2 p, Vector2 v)
        {
            return new Point2(p.X - v.X, p.Y - v.Y);
        }

        /// <summary>
        /// Compare points (<see cref="Point2"/>) for equality.
        /// </summary>
        /// <param name="p1">Point to compare.</param>
        /// <param name="p2">Point to comapre to.</param>
        /// <returns>Comparison result.</returns>
        public static bool Equals(Point2 p1, Point2 p2)
        {
            return p1.Equals(p2);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds <see cref="Point2"/> p to this <see cref="Point2"/> object.
        /// </summary>
        /// <param name="p">Point to add.</param>
        /// <returns>New point.</returns>
        public Point2 Add(Point2 p)
        {
            return Add(this, p);
        }

        /// <summary>
        /// Adds <see cref="Vector2"/> v to this <see cref="Point2"/> object.
        /// </summary>
        /// <param name="v">Vector to add.</param>
        /// <returns>New point.</returns>
        public Point2 Add(Vector2 v)
        {
            return Add(this, v);
        }

        /// <summary>
        /// Subtracts <see cref="Point2"/> p from this <see cref="Point2"/> object.
        /// </summary>
        /// <param name="p">Point to subtract.</param>
        /// <returns>New vector.</returns>
        public Vector2 Subtract(Point2 p)
        {
            return Subtract(this, p);
        }

        /// <summary>
        /// Subtracts <see cref="Vector2"/> v from this <see cref="Point2"/> object.
        /// </summary>
        /// <param name="v">Vector to subtract.</param>
        /// <returns>New point.</returns>
        public Point2 Subtract(Vector2 v)
        {
            return Subtract(this, v);
        }

        /// <summary>
        /// Checks if this <see cref="Point2"/> object is equal to <see cref="Point2"/> p.
        /// </summary>
        /// <param name="p">Point compare to.</param>
        /// <returns>Boolean result of a comparison.</returns>
        public bool Equals(Point2 p)
        {
            if (p == null)
                return false;

            return (_x == p.X) && (_y == p.Y);
        }

        #endregion

        #region Operators

        /// <summary>
        /// Adds <see cref="Point2"/> p2 to <see cref="Point2"/> p1.
        /// </summary>
        /// <param name="p1">Point to add to.</param>
        /// <param name="p2">Point to add.</param>
        /// <returns>New point.</returns>
        public static Point2 operator +(Point2 p1, Point2 p2)
        {
            return Add(p1, p2);
        }

        /// <summary>
        /// Adds <see cref="Vector2"/> v to <see cref="Point2"/> p.
        /// </summary>
        /// <param name="p">Point to add to.</param>
        /// <param name="v">Vector to add.</param>
        /// <returns>New point.</returns>
        public static Point2 operator +(Point2 p, Vector2 v)
        {
            return Add(p, v);
        }

        /// <summary>
        /// Subtracts <see cref="Point2"/> p2 from <see cref="Point2"/> p1.
        /// </summary>
        /// <param name="p1">Point to substrat from.</param>
        /// <param name="p2">Point to subtract.</param>
        /// <returns>New vector.</returns>
        public static Vector2 operator -(Point2 p1, Point2 p2)
        {
            return Subtract(p1, p2);
        }

        /// <summary>
        /// Subtracts <see cref="Vector2"/> v from <see cref="Point2"/> p.
        /// </summary>
        /// <param name="p">Point to subtract from.</param>
        /// <param name="v">Vector to subtract.</param>
        /// <returns>New point.</returns>
        public static Point2 operator -(Point2 p, Vector2 v)
        {
            return Subtract(p, v);
        }

        /// <summary>
        /// Checks if <see cref="Point2"/> p1 is equal to <see cref="Point2"/> p2.
        /// </summary>
        /// <param name="p1">Point compare to.</param>
        /// <param name="p2">Point to compare.</param>
        /// <returns>Boolean result of a comparison.</returns>
        public static bool operator ==(Point2 p1, Point2 p2)
        {
            return p1.Equals(p2);
        }

        /// <summary>
        /// Checks if <see cref="Point2"/> p1 is not equal to <see cref="Point2"/> p2.
        /// </summary>
        /// <param name="p1">Point compare to.</param>
        /// <param name="p2">Point to compare.</param>
        /// <returns>Boolean result of a comparison.</returns>
        public static bool operator !=(Point2 p1, Point2 p2)
        {
            return !p1.Equals(p2);
        }

        #endregion

        #region Override

        /// <summary>
        /// Checks if <see cref="Point2"/> object is equal to <see cref="object"/> obj.
        /// </summary>
        /// <param name="obj">Object compare to.</param>
        /// <returns>Boolean result of a comparison.</returns>
        public override bool Equals(object obj)
        {
            // If parameter cannot be cast to Point2 return false.
            if (!(obj is Point2))
                return false;

            // Return true if the fields match:
            return Equals((Point2)obj);
        }

        /// <summary>
        /// Computes hashcode based on (x,y) coordinates for <see cref="Point2"/>.
        /// </summary>
        /// <returns>Hashcode.</returns>
        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + _x.GetHashCode();
            hash = (hash * 7) + _y.GetHashCode();
            return hash;
        }

        /// <summary>
        /// Prints <see cref="Point2"/> as "Point2(x = {x}, y = {y})".
        /// </summary>
        /// <returns>String representation of the <see cref="Point2"/>.</returns>
        public override string ToString()
        {
            return string.Format("Point2(x = {0}, y = {1})", _x, _y);
        }

        #endregion
    }
}