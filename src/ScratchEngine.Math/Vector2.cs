﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math.Interfaces;
using System;

namespace ScratchEngine.Math
{
    /// <summary>
    /// Struct represents a vector in 2D space (Carthesian).
    /// </summary>
    public struct Vector2 : IEquatable<Vector2>, IMultiplication<Vector2>, IMultiplication<Vector2, float>, IAddition<Vector2>, ISubtraction<Vector2>
    {
        #region Defaults

        /// <summary>
        /// Origin <see cref="Vector2"/> vector.
        /// </summary>
        public static Vector2 Origin { get { return new Vector2(0, 0); } }

        /// <summary>
        /// OX <see cref="Vector2"/> basis vector.
        /// </summary>
        public static Vector2 BasisX { get { return new Vector2(1, 0); } }

        /// <summary>
        /// OY <see cref="Vector2"/> basis vector.
        /// </summary>
        public static Vector2 BasisY { get { return new Vector2(0, 1); } }

        /// <summary>
        /// Unit <see cref="Vector2"/> vector.
        /// </summary>
        public static Vector2 Unit { get { return new Vector2(1, 1); } }

        #endregion

        #region Fields

        private readonly float _x;
        private readonly float _y;

        #endregion

        #region Properties

        /// <summary>
        /// x coordinate.
        /// </summary>
        public float X { get { return _x; } }

        /// <summary>
        /// y coordinate.
        /// </summary>
        public float Y { get { return _y; } }

        /// <summary>
        /// Length (magnitude, norm) of the vector.
        /// </summary>
        public float Length { get { return GetLength(this); } }

        /// <summary>
        /// Shows whether the vector is normalized (it's length is equal to one).
        /// </summary>
        public bool IsNormilized { get { return IsItNormilized(this); } }

        #endregion

        #region Indexer

        /// <summary>
        /// Returns <see cref="float"/> component of <see cref="Vector2"/> vector depending on index.
        /// </summary>
        /// <param name="index">Index of corresponding component.</param>
        /// <returns><see cref="float"/> component.</returns>
        public float this[int index]
        {
            get
            {
                if (index == 0)
                    return _x;
                else if (index == 1)
                    return _y;
                else
                    throw new Math.Exceptions.MathException("");
            }
        }

        #endregion

        #region .ctor

        /// <summary>
        /// Instantiates <see cref="Vector2"/> struct with (x,y) components.
        /// </summary>
        /// <param name="x">x coordinate.</param>
        /// <param name="y">y coordinate.</param>
        public Vector2(float x, float y)
        {
            _x = x;
            _y = y;
        }

        /// <summary>
        /// Instantiates <see cref="Vector2"/> struct with <see cref="float"/> value for all components.
        /// </summary>
        /// <param name="value">All components value.</param>
        public Vector2(float value)
            : this(value, value)
        {
        }

        /// <summary>
        /// Instantiates <see cref="Vector2"/> struct with <see cref="float[]"/> array.
        /// </summary>
        /// <param name="values"></param>
        public Vector2(float[] values)
            : this(values[0], values[1])
        {
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Adds <see cref="Vector2"/> v2 to <see cref="Vector2"/> v1.
        /// </summary>
        /// <param name="v1">Vector to add to.</param>
        /// <param name="v2">Vector to add.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 Add(Vector2 v1, Vector2 v2)
        {
            return new Vector2(v1._x + v2._x, v1._y + v2._y);
        }

        /// <summary>
        /// Adds <see cref="float"/> s to <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to add to.</param>
        /// <param name="s">Scalar to add.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 Add(Vector2 v, float s)
        {
            return new Vector2(v.X + s, v.Y + s);
        }

        /// <summary>
        /// Computes distance between <see cref="Vector2"/> v1 and <see cref="Vector2"/> v2.
        /// </summary>
        /// <param name="v1">Vector from.</param>
        /// <param name="v2">Vector to.</param>
        /// <returns><see cref="float"/> value.</returns>
        public static float Distance(Vector2 v1, Vector2 v2)
        {
            return (float)System.Math.Sqrt((v1._x - v2._x) * (v1._x - v2._x) + (v1._y - v2._y) * (v1._y - v2._y));
        }

        /// <summary>
        /// Divides <see cref="Vector2"/> v1 by <see cref="Vector2"/> v2.
        /// </summary>
        /// <param name="v1">Vector to divide.</param>
        /// <param name="v2">Vector to divide by.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 Divide(Vector2 v1, Vector2 v2)
        {
            return new Vector2(v1.X / v2.X, v1.Y / v2.Y);
        }

        /// <summary>
        /// Divides <see cref="Vector2"/> v by <see cref="float"/> s.
        /// </summary>
        /// <param name="v">Vector to divide.</param>
        /// <param name="s">Scalar to divide by.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 Divide(Vector2 v, float s)
        {
            return new Vector2(v.X / s, v.Y / s);
        }

        /// <summary>
        /// Computes dot product (scalar product or inner product) of <see cref="Vector2"/> v1 and <see cref="Vector2"/> v2.
        /// </summary>
        /// <param name="v1">Vector to multiply.</param>
        /// <param name="v2">Vector to multiply by.</param>
        /// <returns><see cref="float"/> value.</returns>
        public static float DotProduct(Vector2 v1, Vector2 v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y;
        }

        /// <summary>
        /// Compares vectors (<see cref="Vector2"/>) for equality.
        /// </summary>
        /// <param name="v1">Vector to compare.</param>
        /// <param name="v2">Vector to comapre to.</param>
        /// <returns><see cref="bool"/> value.</returns>
        public static bool Equals(Vector2 v1, Vector2 v2)
        {
            if (v1 == null || v2 == null)
                return false;

            return (v1._x == v2._x) && (v1._y == v2._y);
        }

        /// <summary>
        /// Computes hashcode for <see cref="Vector2"/> v instance.
        /// </summary>
        /// <param name="v">Vector to compute from.</param>
        /// <returns><see cref="int"/> value.</returns>
        public static int GetHashCode(Vector2 v)
        {
            int hash = 13;
            hash = (hash * 7) + v._x.GetHashCode();
            hash = (hash * 7) + v._y.GetHashCode();
            return hash;
        }

        /// <summary>
        /// Computes length (magnitude, norm) of <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to compute from.</param>
        /// <returns><see cref="float"/> value.</returns>
        public static float GetLength(Vector2 v)
        {
            return (float)System.Math.Sqrt(v.X * v.X + v.Y * v.Y);
        }

        /// <summary>
        /// Finds maximal vector between <see cref="Vector2"/> v1 and <see cref="Vector2"/> v2 by vector's length.
        /// </summary>
        /// <param name="v1">Vector to compare.</param>
        /// <param name="v2">Vector to compare to.</param>
        /// <returns>Maximal vector.</returns>
        public static Vector2 Max(Vector2 v1, Vector2 v2)
        {
            if (v1.Length > v2.Length)
                return v1;
            else
                return v2;
        }

        /// <summary>
        /// Finds minimal vector between <see cref="Vector2"/> v1 and <see cref="Vector2"/> v2 by vector's length.
        /// </summary>
        /// <param name="v1">Vector to compare.</param>
        /// <param name="v2">Vector to compare to.</param>
        /// <returns>Minimal vector.</returns>
        public static Vector2 Min(Vector2 v1, Vector2 v2)
        {
            if (v1.Length < v2.Length)
                return v1;
            else
                return v2;
        }

        /// <summary>
        /// Multiplies <see cref="Vector2"/> v1 by <see cref="Vector2"/> v2.
        /// </summary>
        /// <param name="v1">Vector to multiply.</param>
        /// <param name="v2">Scalar to multiply by.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 Multiply(Vector2 v1, Vector2 v2)
        {
            return new Vector2(v1.X * v2.X, v1.Y * v2.Y);
        }

        /// <summary>
        /// Multiplies <see cref="Vector2"/> v by <see cref="float"/> scalar s.
        /// </summary>
        /// <param name="v">Vector to multiply.</param>
        /// <param name="s">Scalar to multiply by.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 Multiply(Vector2 v, float s)
        {
            return new Vector2(v.X * s, v.Y * s);
        }

        /// <summary>
        /// Negates <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to negates.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 Negate(Vector2 v)
        {
            return new Vector2(-v._x, -v._y);
        }

        /// <summary>
        /// Checks whether the <see cref="Vector2"/> v is normalized (it's length is equal to one).
        /// </summary>
        /// <param name="v">Vector to check.</param>
        /// <returns><see cref="bool"/> result.</returns>
        public static bool IsItNormilized(Vector2 v)
        {
            return GetLength(v) == 1;
        }

        /// <summary>
        /// Normilizes <see cref="Vector2"/> v (divide all components by length).
        /// </summary>
        /// <param name="v">Vector to normilize.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 Normalize(Vector2 v)
        {
            float length = GetLength(v);
            return new Vector2(v._x / length, v._y / length);
        }

        /// <summary>
        /// Reflects <see cref="Vector2"/> vector v in <see cref="Vector2"/> normal n.
        /// </summary>
        /// <param name="v">Vector to normilize.</param>
        /// <param name="n">Normal for reflection.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 Reflect(Vector2 v, Vector2 n)
        {
            return 2 * DotProduct(v, n) * n - v;
        }

        /// <summary>
        /// Subtracts <see cref="Vector2"/> v2 from <see cref="Vector2"/> v1.
        /// </summary>
        /// <param name="v1">Vector to substrat from.</param>
        /// <param name="v2">Vector to subtract.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 Subtract(Vector2 v1, Vector2 v2)
        {
            return new Vector2(v1.X - v2.X, v1.Y - v2.Y);
        }

        /// <summary>
        /// Subtracts <see cref="float"/> s from <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to substrat from.</param>
        /// <param name="s">Scalar to subtract.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 Subtract(Vector2 v, float s)
        {
            return new Vector2(v.X - s, v.Y - s);
        }

        /// <summary>
        /// Returns <see cref="float[]"/> representation of <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to represent.</param>
        /// <returns><see cref="float[]"/> array.</returns>
        public static float[] ToArray(Vector2 v)
        {
            return new float[] { v._x, v._y };
        }

        /// <summary>
        /// Returns <see cref="Point2"/> representation of <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to represent.</param>
        /// <returns>New <see cref="Point2"/> point.</returns>
        public static Point2 ToPoint2(Vector2 v)
        {
            return new Point2(v._x, v._y);
        }

        /// <summary>
        /// Returns <see cref="string"/> representation "Vector2(x = {x}, y = {y})" of <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to represent.</param>
        /// <returns><see cref="string"/> value.</returns>
        public static string ToString(Vector2 v)
        {
            return string.Format("Vector2(x = {0}, y = {1})", v._x, v._y);
        }

        /// <summary>
        /// Returns <see cref="Vector3"/> representation of <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to represent.</param>
        /// <returns>New <see cref="Vector3"/> vector.</returns>
        public static Vector3 ToVector3(Vector2 v)
        {
            return new Vector3(v._x, v._y, 0);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds <see cref="Vector2"/> v to this <see cref="Vector2"/> object.
        /// </summary>
        /// <param name="v">Vector to add.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public Vector2 Add(Vector2 v)
        {
            return Add(this, v);
        }

        /// <summary>
        /// Adds <see cref="float"/> s to this <see cref="Vector2"/> object.
        /// </summary>
        /// <param name="s">Scalar to add.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public Vector2 Add(float s)
        {
            return Add(this, s);
        }

        /// <summary>
        /// Computes distance between this <see cref="Vector2"/> object and <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to.</param>
        /// <returns><see cref="float"/> value.</returns>
        public float Distance(Vector2 v)
        {
            return Distance(this, v);
        }

        /// <summary>
        /// Divides this <see cref="Vector2"/> object by <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to divide by.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public Vector2 Divide(Vector2 v)
        {
            return Divide(this, v);
        }

        /// <summary>
        /// Divides this <see cref="Vector2"/> object by <see cref="float"/> s.
        /// </summary>
        /// <param name="s">Scalar to divide by.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public Vector2 Divide(float s)
        {
            return Divide(this, s);
        }

        /// <summary>
        /// Computes dot product (scalar product or inner product) of this <see cref="Vector2"/> object and <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to multiply by.</param>
        /// <returns><see cref="float"/> value.</returns>
        public float DotProduct(Vector2 v)
        {
            return DotProduct(this, v);
        }

        /// <summary>
        /// Compares vectors (<see cref="Vector2"/>) for equality.
        /// </summary>
        /// <param name="v">Vector compare to.</param>
        /// <returns><see cref="bool"/> value.</returns>
        public bool Equals(Vector2 v)
        {
            return Equals(this, v);
        }

        /// <summary>
        /// Multiplies this <see cref="Vector2"/> object by <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to multiply by.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public Vector2 Multiply(Vector2 v)
        {
            return Multiply(this, v);
        }

        /// <summary>
        /// Multiplies this <see cref="Vector2"/> object by <see cref="float"/> scalar s.
        /// </summary>
        /// <param name="s">Scalar to multiply by.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public Vector2 Multiply(float s)
        {
            return Multiply(this, s);
        }

        /// <summary>
        /// Negates this <see cref="Vector2"/> object.
        /// </summary>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public Vector2 Negate()
        {
            return Negate(this);
        }

        /// <summary>
        /// Normilizes this <see cref="Vector2"/> object (divide all components by length).
        /// </summary>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public Vector2 Normalize()
        {
            return Normalize(this);
        }

        /// <summary>
        /// Reflects this <see cref="Vector2"/> object in <see cref="Vector2"/> normal n.
        /// </summary>
        /// <param name="n">Normal for reflection.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public Vector2 Reflect(Vector2 n)
        {
            return Reflect(this, n);
        }

        /// <summary>
        /// Subtracts <see cref="Vector2"/> v from this <see cref="Vector2"/> object.
        /// </summary>
        /// <param name="v">Vector to subtract.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public Vector2 Subtract(Vector2 v)
        {
            return Subtract(this, v);
        }

        /// <summary>
        /// Subtracts <see cref="float"/> s from this <see cref="Vector2"/> object.
        /// </summary>
        /// <param name="s">Scalar to substract.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public Vector2 Subtract(float s)
        {
            return Subtract(this, s);
        }

        /// <summary>
        /// Returns <see cref="float[]"/> representation of this <see cref="Vector2"/> object.
        /// </summary>
        /// <returns><see cref="float[]"/> array.</returns>
        public float[] ToArray()
        {
            return ToArray(this);
        }

        /// <summary>
        /// Returns <see cref="Point2"/> representation of this <see cref="Vector2"/> object.
        /// </summary>
        /// <returns>New <see cref="Point2"/> point.</returns>
        public Point2 ToPoint2()
        {
            return ToPoint2(this);
        }

        /// <summary>
        /// Returns <see cref="Vector3"/> representation of this <see cref="Vector2"/> object.
        /// </summary>
        /// <returns>New <see cref="Vector3"/> vector.</returns>
        public Vector3 ToVector3()
        {
            return ToVector3(this);
        }

        #endregion

        #region Operators

        /// <summary>
        /// Adds <see cref="Vector2"/> v2 to <see cref="Vector2"/> v1.
        /// </summary>
        /// <param name="v1">Vector to add to.</param>
        /// <param name="v2">Vector to add.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 operator +(Vector2 v1, Vector2 v2)
        {
            return Add(v1, v2);
        }

        /// <summary>
        /// Adds <see cref="float"/> s to <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to add to.</param>
        /// <param name="s">Scalar to add.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 operator +(Vector2 v, float s)
        {
            return Add(v, s);
        }

        /// <summary>
        /// Adds <see cref="Vector2"/> v to <see cref="float"/> s.
        /// </summary>
        /// <param name="s">Scalar to add to.</param>
        /// <param name="v">Vector to add.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 operator +(float s, Vector2 v)
        {
            return Add(v, s);
        }

        /// <summary>
        /// Subtracts <see cref="Vector2"/> v2 from <see cref="Vector2"/> v1.
        /// </summary>
        /// <param name="v1">Vector substract from.</param>
        /// <param name="v2">Vector to substract.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 operator -(Vector2 v1, Vector2 v2)
        {
            return Subtract(v1, v2);
        }

        /// <summary>
        /// Subtracts <see cref="float"/> s from <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector substract from.</param>
        /// <param name="s">Scalar to substract.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 operator -(Vector2 v, float s)
        {
            return Subtract(v, s);
        }

        /// <summary>
        /// Subtracts <see cref="Vector2"/> v from <see cref="float"/> s.
        /// </summary>
        /// <param name="s">Scalar to substract from.</param>
        /// <param name="v">Vector substract.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 operator -(float s, Vector2 v)
        {
            return -Subtract(v, s);
        }

        /// <summary>
        /// Negates <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="v">Vector to negate.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 operator -(Vector2 v)
        {
            return Negate(v);
        }

        /// <summary>
        /// Multiplies <see cref="Vector2"/> v1 by <see cref="Vector2"/> v2.
        /// </summary>
        /// <param name="v1">Vector to multiply.</param>
        /// <param name="v2">Vector to multiply by.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 operator *(Vector2 v1, Vector2 v2)
        {
            return Multiply(v1, v2);
        }

        /// <summary>
        /// Multiplies <see cref="Vector2"/> v by <see cref="float"/> s.
        /// </summary>
        /// <param name="v">Vector to multiply.</param>
        /// <param name="s">Scalar to multiply by.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 operator *(Vector2 v, float s)
        {
            return Multiply(v, s);
        }

        /// <summary>
        /// Multiplies <see cref="float"/> s by <see cref="Vector2"/> v.
        /// </summary>
        /// <param name="s">Scalar to multiply.</param>
        /// <param name="v">Vector to multiply by.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 operator *(float s, Vector2 v)
        {
            return Multiply(v, s);
        }

        /// <summary>
        /// Divides <see cref="Vector2"/> v1 by <see cref="Vector2"/> v2.
        /// </summary>
        /// <param name="v1">Vector to divide.</param>
        /// <param name="v2">Vector to divide by.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 operator /(Vector2 v1, Vector2 v2)
        {
            return Divide(v1, v2);
        }

        /// <summary>
        /// Divides <see cref="Vector2"/> v by <see cref="float"/> s.
        /// </summary>
        /// <param name="v">Vector to divide.</param>
        /// <param name="s">Scalar to divide by.</param>
        /// <returns>New <see cref="Vector2"/> vector.</returns>
        public static Vector2 operator /(Vector2 v, float s)
        {
            return Divide(v, s);
        }

        /// <summary>
        /// Compares vectors (<see cref="Vector2"/>) for equality.
        /// </summary>
        /// <param name="v1">Vector to compare.</param>
        /// <param name="v2">Vector to compare to.</param>
        /// <returns><see cref="bool"/> value.</returns>
        public static bool operator ==(Vector2 v1, Vector2 v2)
        {
            return Equals(v1, v2);
        }

        /// <summary>
        /// Compares vectors (<see cref="Vector2"/>) for inequality.
        /// </summary>
        /// <param name="v1">Vector to compare.</param>
        /// <param name="v2">Vector to compare to.</param>
        /// <returns><see cref="bool"/> value.</returns>
        public static bool operator !=(Vector2 v1, Vector2 v2)
        {
            return !Equals(v1, v2);
        }

        /// <summary>
        /// Casts <see cref="Vector2"/> v to <see cref="float[]"/> array;
        /// </summary>
        /// <param name="v">Vector to represent.</param>
        public static explicit operator float[](Vector2 v)
        {
            return ToArray(v);
        }

        /// <summary>
        /// Casts <see cref="Vector2"/> v to <see cref="Point2"/> object;
        /// </summary>
        /// <param name="v">Vector to cast.</param>
        public static explicit operator Point2(Vector2 v)
        {
            return ToPoint2(v);
        }

        /// <summary>
        /// Casts <see cref="Vector2"/> v to <see cref="Vector3"/> object;
        /// </summary>
        /// <param name="v">Vector to cast.</param>
        public static explicit operator Vector3(Vector2 v)
        {
            return ToVector3(v);
        }

        #endregion

        #region Override

        /// <summary>
        /// Compares this <see cref="Vector2"/> object with <see cref="object"/> obj for equality.
        /// </summary>
        /// <param name="obj">Object to compare to.</param>
        /// <returns><see cref="bool"/> value.</returns>
        public override bool Equals(object obj)
        {
            if (!(obj is Vector2))
                return false;

            return Equals((Vector2)obj);
        }

        /// <summary>
        /// Computes hashcode for this <see cref="Vector2"/> object.
        /// </summary>
        /// <returns><see cref="int"/> value.</returns>
        public override int GetHashCode()
        {
            return GetHashCode(this);
        }

        /// <summary>
        /// Returns <see cref="string"/> representation "Vector2(x = {x}, y = {y})" of this <see cref="Vector2"/> object.
        /// </summary>
        /// <returns><see cref="string"/> value.</returns>
        public override string ToString()
        {
            return ToString(this);
        }

        #endregion
    }
}