﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

namespace ScratchEngine.Math
{
    /// <summary>
    /// 
    /// </summary>
    public static class NumericExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static double ToRadians(this double val)
        {
            return (System.Math.PI / 180) * val;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="val"></param>
        /// <returns></returns>
        public static float ToRadians(this float val)
        {
            return (float)(System.Math.PI / 180) * val;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static Vector2 ToVector2(this float[] arr)
        {
            if (arr.Length != 2)
                throw new Math.Exceptions.MathException("Array length must be equal to 2");

            return new Vector2(arr[0], arr[1]);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="arr"></param>
        /// <returns></returns>
        public static Vector3 ToVector3(this float[] arr)
        {
            if (arr.Length != 3 && arr.Length != 4)
                throw new Math.Exceptions.MathException("Array length must be equal to 3");

            return new Vector3(arr[0], arr[1], arr[2]);
        }
    }
}