﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math.Interfaces;
using System;

namespace ScratchEngine.Math
{
    /// <summary>
    /// Struct represents a vector in 3D space (Carthesian).
    /// </summary>
    public struct Vector3 : IEquatable<Vector3>, IMultiplication<Vector3>, IMultiplication<Vector3, float>, IAddition<Vector3>, ISubtraction<Vector3>
    {
        #region Defaults

        /// <summary>
        /// Origin <see cref="Vector3"/> vector.
        /// </summary>
        public static Vector3 Origin { get { return new Vector3(0, 0, 0); } }

        /// <summary>
        /// OX <see cref="Vector3"/> basis vector.
        /// </summary>
        public static Vector3 BasisX { get { return new Vector3(1, 0, 0); } }

        /// <summary>
        /// OY <see cref="Vector3"/> basis vector.
        /// </summary>
        public static Vector3 BasisY { get { return new Vector3(0, 1, 0); } }

        /// <summary>
        /// OZ <see cref="Vector3"/> basis vector.
        /// </summary>
        public static Vector3 BasisZ { get { return new Vector3(0, 0, 1); } }

        #endregion

        #region Fields

        private readonly float _x;
        private readonly float _y;
        private readonly float _z;

        #endregion

        #region Properties

        /// <summary>
        /// x coordinate.
        /// </summary>
        public float X { get { return _x; } }

        /// <summary>
        /// y coordinate.
        /// </summary>
        public float Y { get { return _y; } }

        /// <summary>
        /// z coordinate.
        /// </summary>
        public float Z { get { return _z; } }

        /// <summary>
        /// Length (magnitude, norm) of the vector.
        /// </summary>
        public float Length { get { return GetLength(this); } }

        #endregion

        #region .ctor

        /// <summary>
        /// Instantiates <see cref="Vector3"/> struct with (x,y,z) coordinates.
        /// </summary>
        /// <param name="x">x coordinate.</param>
        /// <param name="y">y coordinate.</param>
        /// <param name="z">z coordinate.</param>
        public Vector3(float x, float y, float z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public Vector3(Vector3 v)
            : this(v._x, v._y, v._z)
        {
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static float GetLength(Vector3 v)
        {
            return (float)System.Math.Sqrt(v.X * v.X + v.Y * v.Y + v.Z * v.Z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector3 Normalize(Vector3 v)
        {
            float length = GetLength(v);
            return new Vector3(v._x / length, v._y / length, v._z / length);
        }

        /// <summary>
        /// Adds <see cref="Vector3"/> v2 to <see cref="Vector3"/> v1.
        /// </summary>
        /// <param name="v1">Vector to add to.</param>
        /// <param name="v2">Vector to add.</param>
        /// <returns>New <see cref="Vector3"/> vector.</returns>
        public static Vector3 Add(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.X + v2.X, v1.Y + v2.Y, v1.Z + v2.Z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Vector3 Add(Vector3 v, float s)
        {
            return new Vector3(v.X + s, v.Y + s, v.Z + s);
        }

        /// <summary>
        /// Subtracts <see cref="Vector3"/> v2 from <see cref="Vector3"/> v1.
        /// </summary>
        /// <param name="v1">Vector to substrat from.</param>
        /// <param name="v2">Vector to subtract.</param>
        /// <returns>New <see cref="Vector3"/> vector.</returns>
        public static Vector3 Subtract(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.X - v2.X, v1.Y - v2.Y, v1.Z - v2.Z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Vector3 Subtract(Vector3 v, float s)
        {
            return new Vector3(v.X - s, v.Y - s, v.Z - s);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static Vector3 Multiply(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.X * v2.X, v1.Y * v2.Y, v1.Z * v2.Z);
        }

        /// <summary>
        /// Multiplies <see cref="Vector3"/> v with <see cref="float"/> scalar s.
        /// </summary>
        /// <param name="v">Vector to multiply with.</param>
        /// <param name="s">Scalar to multiply.</param>
        /// <returns>New <see cref="Vector3"/> vector.</returns>
        public static Vector3 Multiply(Vector3 v, float s)
        {
            return new Vector3(v.X * s, v.Y * s, v.Z * s);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static Vector3 Multiply(Vector3 v, Matrix4 m)
        {
            var vector = MatrixUtilities.Multiply(v.ToArray(), m.ToArray());
            return vector.ToVector3();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static Vector3 Divide(Vector3 v1, Vector3 v2)
        {
            return new Vector3(v1.X / v2.X, v1.Y / v2.Y, v1.Z / v2.Z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Vector3 Divide(Vector3 v, float s)
        {
            return new Vector3(v.X / s, v.Y / s, v.Z / s);
        }

        /// <summary>
        /// Dot product (scalar product or inner product) of <see cref="Vector3"/> v1 and <see cref="Vector3"/> v2.
        /// </summary>
        /// <param name="v1">Vector to multiply with.</param>
        /// <param name="v2">Vector to multiply.</param>
        /// <returns><see cref="float"/> value.</returns>
        public static float DotProduct(Vector3 v1, Vector3 v2)
        {
            return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
        }

        /// <summary>
        /// Cross product (vector product) of <see cref="Vector3"/> v1 and <see cref="Vector3"/> v2.
        /// </summary>
        /// <param name="v1">Vector to multiply with.</param>
        /// <param name="v2">Vector to multiply.</param>
        /// <returns>New <see cref="Vector3"/> vector.</returns>
        public static Vector3 CrossProduct(Vector3 v1, Vector3 v2)
        {
            //P×Q = PyQz − PzQy ,PzQx − PxQz ,PxQy − PyQx.
            float x = v1.Y * v2.Z - v1.Z * v2.Y;
            float y = v1.Z * v2.X - v1.X * v2.Z;
            float z = v1.X * v2.Y - v1.Y * v2.X;
            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Compares vectors (<see cref="Vector3"/>) for equality.
        /// </summary>
        /// <param name="v1">Vector to compare.</param>
        /// <param name="v2">Vector comapre to.</param>
        /// <returns>Boolean result of a check.</returns>
        public static bool Equals(Vector3 v1, Vector3 v2)
        {
            if (v1 == null || v2 == null)
                return false;

            return (v1._x == v2._x) && (v1._y == v2._y) && (v1._z == v2._z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static int GetHashCode(Vector3 v)
        {
            int hash = 13;
            hash = (hash * 7) + v._x.GetHashCode();
            hash = (hash * 7) + v._y.GetHashCode();
            hash = (hash * 7) + v._z.GetHashCode();
            return hash;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="angle">radians</param>
        /// <param name="axis"></param>
        /// <returns></returns>
        public Vector3 Rotate(float angle, Vector3 axis)
        {
            var rotation = Quaternion.GetLocalRotationQuaternion(angle, axis);
            var conjugate = rotation.Conjugate();

            var w = rotation * ToQuaternion(this) * conjugate;

            return new Vector3(w.X, w.Y, w.Z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Quaternion ToQuaternion(Vector3 v)
        {
            return new Quaternion(0, v._x, v._y, v._z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static float[] ToArray(Vector3 v)
        {
            return new float[] { v._x, v._y, v._z, 1 };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static string ToString(Vector3 v)
        {
            return string.Format("Vector3(x = {0}, y = {1}, z = {2})", v._x, v._y, v._z);
        }

        #endregion

        #region Methods

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public Vector3 Normalize()
        {
            return Normalize(this);
        }

        /// <summary>
        /// Adds <see cref="Vector3"/> v to this <see cref="Vector3"/> object.
        /// </summary>
        /// <param name="v">Vector to add.</param>
        /// <returns>New <see cref="Vector3"/> vector.</returns>
        public Vector3 Add(Vector3 v)
        {
            return Add(this, v);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public Vector3 Add(float s)
        {
            return Add(this, s);
        }

        /// <summary>
        /// Subtracts <see cref="Vector3"/> v from this <see cref="Vector3"/> object.
        /// </summary>
        /// <param name="v">Vector to subtract.</param>
        /// <returns>New <see cref="Vector3"/> vector.</returns>
        public Vector3 Subtract(Vector3 v)
        {
            return Subtract(this, v);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public Vector3 Subtract(float s)
        {
            return Subtract(this, s);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public Vector3 Multiply(Vector3 v)
        {
            return Multiply(this, v);
        }

        /// <summary>
        /// Multiplies this <see cref="Vector32"/> object with <see cref="float"/> scalar s.
        /// </summary>
        /// <param name="s">Scalar to multiply.</param>
        /// <returns>New <see cref="Vector3"/> vector.</returns>
        public Vector3 Multiply(float s)
        {
            return Multiply(this, s);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public Vector3 Multiply(Matrix4 m)
        {
            return Multiply(this, m);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public Vector3 Divide(Vector3 v)
        {
            return Divide(this, v);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public Vector3 Divide(float s)
        {
            return Divide(this, s);
        }

        /// <summary>
        /// Dot product (scalar product or inner product) of this <see cref="Vector3"/> object and <see cref="Vector3"/> v.
        /// </summary>
        /// <param name="v">Vector to multiply.</param>
        /// <returns><see cref="float"/> value.</returns>
        public float DotProduct(Vector3 v)
        {
            return DotProduct(this, v);
        }

        /// <summary>
        /// Cross product (vector product) of <see cref="Vector3"/> v1 and <see cref="Vector3"/> v2.
        /// </summary>
        /// <param name="v">Vector to multiply.</param>
        /// <returns>New <see cref="Vector3"/> vector.</returns>
        public Vector3 CrossProduct(Vector3 v)
        {
            return CrossProduct(this, v);
        }

        /// <summary>
        /// Checks if this <see cref="Vector3"/> object is equal to <see cref="Vector3"/> v.
        /// </summary>
        /// <param name="v">Vector compare to.</param>
        /// <returns>Boolean result of a check.</returns>
        public bool Equals(Vector3 v)
        {
            return Equals(this, v);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public float[] ToArray()
        {
            return ToArray(this);
        }

        #endregion

        #region Operators

        /// <summary>
        /// Adds <see cref="Vector3"/> v2 to <see cref="Vector3"/> v1.
        /// </summary>
        /// <param name="v1">Vector to add to.</param>
        /// <param name="v2">Vector to add.</param>
        /// <returns>New <see cref="Vector3"/> vector.</returns>
        public static Vector3 operator +(Vector3 v1, Vector3 v2)
        {
            return Add(v1, v2);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Vector3 operator +(Vector3 v, float s)
        {
            return Add(v, s);
        }

        /// <summary>
        /// Subtracts <see cref="Vector3"/> v2 from <see cref="Vector3"/> v1.
        /// </summary>
        /// <param name="v1">Vector substract from.</param>
        /// <param name="v2">Vector to substract.</param>
        /// <returns>New <see cref="Vector3"/> vector.</returns>
        public static Vector3 operator -(Vector3 v1, Vector3 v2)
        {
            return Subtract(v1, v2);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Vector3 operator -(Vector3 v, float s)
        {
            return Subtract(v, s);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector3 operator -(Vector3 v)
        {
            return new Vector3(-v.X, -v.Y, -v.Z);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static Vector3 operator *(Vector3 v1, Vector3 v2)
        {
            return Multiply(v1, v2);
        }

        /// <summary>
        /// Multiplies <see cref="Vector3"/> v with <see cref="float"/> scalar s.
        /// </summary>
        /// <param name="v">Vector to multiply with.</param>
        /// <param name="s">Scalar to multiply.</param>
        /// <returns>New <see cref="Vector3"/> vector.</returns>
        public static Vector3 operator *(Vector3 v, float s)
        {
            return Multiply(v, s);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="m"></param>
        /// <returns></returns>
        public static Vector3 operator *(Vector3 v, Matrix4 m)
        {
            return Multiply(v, m);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v1"></param>
        /// <param name="v2"></param>
        /// <returns></returns>
        public static Vector3 operator /(Vector3 v1, Vector3 v2)
        {
            return Divide(v1, v2);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public static Vector3 operator /(Vector3 v, float s)
        {
            return Divide(v, s);
        }

        /// <summary>
        /// Checks if <see cref="Vector3"/> v1 is equal to <see cref="Vector3"/> v2.
        /// </summary>
        /// <param name="v1">Vector to compare.</param>
        /// <param name="v2">Vector compare to.</param>
        /// <returns>Boolean result of a check.</returns>
        public static bool operator ==(Vector3 v1, Vector3 v2)
        {
            return Equals(v1, v2);
        }

        /// <summary>
        /// Checks if <see cref="Vector3"/> v1 is not equal to <see cref="Vector3"/> v2.
        /// </summary>
        /// <param name="v1">Vector to compare.</param>
        /// <param name="v2">Vector compare to.</param>
        /// <returns>Boolean result of a check.</returns>
        public static bool operator !=(Vector3 v1, Vector3 v2)
        {
            return !Equals(v1, v2);
        }

        #endregion

        #region Override

        /// <summary>
        /// Checks if this <see cref="Vector3"/> object is equal to <see cref="object"/> obj.
        /// </summary>
        /// <param name="obj">Object compare to.</param>
        /// <returns>Boolean result of a check.</returns>
        public override bool Equals(object obj)
        {
            // If parameter cannot be cast to Vector3 return false.
            if (!(obj is Vector3))
                return false;

            // Return true if the fields match:
            return Equals((Vector3)obj);
        }

        /// <summary>
        /// Computes hashcode based on (x,y,z) coordinates for <see cref="Vector3"/>.
        /// </summary>
        /// <returns>Hashcode.</returns>
        public override int GetHashCode()
        {
            return GetHashCode(this);
        }

        /// <summary>
        /// Prints <see cref="Vector3"/> as "Vector3(x = {x}, y = {y}, z = {z})".
        /// </summary>
        /// <returns>String representation of the <see cref="Vector3"/>.</returns>
        public override string ToString()
        {
            return ToString(this);
        }

        #endregion
    }
}