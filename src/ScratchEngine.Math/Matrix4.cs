﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math.Interfaces;
using System;

namespace ScratchEngine.Math
{
    /// <summary>
    /// 
    /// </summary>
    public struct Matrix4 : IMultiplication<Matrix4>
    {
        #region Defaults

        /// <summary>
        /// Returns empty 4x4 matrix.
        /// </summary>
        public static Matrix4 Empty { get { return new Matrix4(MatrixUtilities.GetEmpty(GetLength(), GetLength())); } }

        /// <summary>
        /// Returns 4x4 identity matrix.
        /// </summary>
        public static Matrix4 Identity { get { return new Matrix4(MatrixUtilities.GetIdentity(GetLength())); } }

        #endregion

        #region Fields

        private float[,] _matrix;

        #endregion

        #region Indexer

        /// <summary>
        /// Gets/Sets float number at a position.
        /// </summary>
        /// <param name="i">Row index.</param>
        /// <param name="j">Column index.</param>
        /// <returns><see cref="float"/> number.</returns>
        public float this[int i, int j]
        {
            get
            {
                return _matrix[i, j];
            }
            set
            {
                _matrix[i, j] = value;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Length of the matrix in both dimensions.
        /// </summary>
        public int Length { get { return GetLength(); } }

        #endregion

        #region .ctor

        /// <summary>
        /// Instantiates <see cref="Matrix4"/> struct with float[4,4] array.
        /// </summary>
        /// <param name="matrix">float[4,4] array.</param>
        public Matrix4(float[,] matrix)
        {
            if (matrix.GetLength(0) != GetLength() || matrix.GetLength(1) != GetLength())
                throw new Exception("Matrix dimensions are not correct.");

            _matrix = matrix;
        }

        #endregion

        #region Static

        /// <summary>
        /// Returns length of the matrix in both dimensions.
        /// </summary>
        public static int GetLength() { return 4; }

        /// <summary>
        /// Multiplies <see cref="Matrix4"/> m1 with <see cref="Matrix4"/> m2.
        /// </summary>
        /// <param name="m1">Matrix to multiply with.</param>
        /// <param name="m2">Matrix to multiply.</param>
        /// <returns>New <see cref="Matrix4"/> matrix.</returns>
        public static Matrix4 Multiply(Matrix4 m1, Matrix4 m2)
        {
            return new Matrix4(MatrixUtilities.Multiply(m1.ToArray(), m2.ToArray()));
        }

        public static Vector3 Multiply(Matrix4 m, Vector3 v)
        {
            var vector = MatrixUtilities.Multiply(m.ToArray(), v.ToArray());
            return vector.ToVector3();
        }

        /// <summary>
        /// Adds <see cref="Matrix4"/> m2 to <see cref="Matrix4"/> m1.
        /// </summary>
        /// <param name="m1">Matrix to add to.</param>
        /// <param name="m2">Matrix to add.</param>
        /// <returns>New <see cref="Matrix4"/> matrix.</returns>
        public static Matrix4 Add(Matrix4 m1, Matrix4 m2)
        {
            return new Matrix4(MatrixUtilities.Add(m1.ToArray(), m2.ToArray()));
        }

        /// <summary>
        /// Subtracts <see cref="Matrix4"/> m2 from <see cref="Matrix4"/> m1.
        /// </summary>
        /// <param name="m1">Matrix to subtract from.</param>
        /// <param name="m2">Matrix to subtract.</param>
        /// <returns>New <see cref="Matrix4"/> matrix.</returns>
        public static Matrix4 Subtract(Matrix4 m1, Matrix4 m2)
        {
            return new Matrix4(MatrixUtilities.Substract(m1.ToArray(), m2.ToArray()));
        }

        /// <summary>
        /// Transposes <see cref="Matrix4"/> m
        /// </summary>
        /// <param name="m">Matrix to transpose</param>
        /// <returns>>New <see cref="Matrix4"/> matrix.</returns>
        public static Matrix4 Transpose(Matrix4 m)
        {
            return new Matrix4(MatrixUtilities.Transpose(m.ToArray()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static float[,] ToArray(Matrix4 m)
        {
            return m._matrix;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Multiplies <see cref="Matrix4"/> matrix with this <see cref="Matrix4"/> object.
        /// </summary>
        /// <param name="matrix">Matrix to multiply.</param>
        /// <returns>New <see cref="Matrix4"/> matrix.</returns>
        public Matrix4 Multiply(Matrix4 matrix)
        {
            return Multiply(this, matrix);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="vector"></param>
        /// <returns></returns>
        public Vector3 Multiply(Vector3 vector)
        {
            return Multiply(this, vector);
        }

        /// <summary>
        /// Adds <see cref="Matrix4"/> matrix to this <see cref="Matrix4"/> object.
        /// </summary>
        /// <param name="matrix">Matrix to add.</param>
        /// <returns>New <see cref="Matrix4"/> matrix.</returns>
        public Matrix4 Add(Matrix4 matrix)
        {
            return Add(this, matrix);
        }

        /// <summary>
        /// Subtracts <see cref="Matrix4"/> matrix from this <see cref="Matrix4"/> object.
        /// </summary>
        /// <param name="matrix">Matrix to substract.</param>
        /// <returns>New <see cref="Matrix4"/> matrix.</returns>
        public Matrix4 Subtract(Matrix4 matrix)
        {
            return Subtract(this, matrix);
        }

        /// <summary>
        /// Transposes this <see cref="Matrix4"/> object.
        /// </summary>
        /// <returns>New <see cref="Matrix4"/> matrix.</returns>
        public Matrix4 Transpose()
        {
            return Transpose(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public float[,] ToArray()
        {
            return _matrix;
        }

        #endregion

        #region Operators

        /// <summary>
        /// Multiplies <see cref="Matrix4"/> m1 with <see cref="Matrix4"/> m2.
        /// </summary>
        /// <param name="m1">Matrix multiply with.</param>
        /// <param name="m2">Matrix to multiply.</param>
        /// <returns>New <see cref="Matrix4"/> matrix.</returns>
        public static Matrix4 operator *(Matrix4 m1, Matrix4 m2)
        {
            return Multiply(m1, m2);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <param name="v"></param>
        /// <returns></returns>
        public static Vector3 operator *(Matrix4 m, Vector3 v)
        {
            return Multiply(m, v);
        }

        /// <summary>
        /// Adds <see cref="Matrix4"/> m2 to <see cref="Matrix4"/> m1.
        /// </summary>
        /// <param name="m1">Matrix to add to.</param>
        /// <param name="m2">Matrix to add.</param>
        /// <returns>New <see cref="Matrix4"/> matrix.</returns>
        public static Matrix4 operator +(Matrix4 m1, Matrix4 m2)
        {
            return Add(m1, m2);
        }

        /// <summary>
        /// Subtracts <see cref="Matrix4"/> m2 from <see cref="Matrix4"/> m1.
        /// </summary>
        /// <param name="m1">Matrix to substract from.</param>
        /// <param name="m2">Matrix to substract.</param>
        /// <returns>New <see cref="Matrix4"/> matrix.</returns>
        public static Matrix4 operator -(Matrix4 m1, Matrix4 m2)
        {
            return Subtract(m1, m2);
        }

        #endregion

        #region Override

        /// <summary>
        /// Prints <see cref="Matrix4"/> as "Matrix4 [ m11 m12 m13 m14 ]".
        ///                                          [ m21 m22 m23 m24 ]
        ///                                          [ m31 m32 m33 m34 ]
        ///                                          [ m41 m42 m43 m44 ]
        /// </summary>
        /// <returns>String representation of the <see cref="Matrix4"/>.</returns>
        public override string ToString()
        {
            return string.Format("Matrix4 [ {0} {1} {2} {3} ] {4} [ {5} {6} {7} {8} ] {9} [ {10} {11} {12} {13} ] {14} [ {15} {16} {17} {18} ]",
                _matrix[0, 0], _matrix[0, 1], _matrix[0, 2], _matrix[0, 3], Environment.NewLine,
                _matrix[1, 0], _matrix[1, 1], _matrix[1, 2], _matrix[1, 3], Environment.NewLine,
                _matrix[2, 0], _matrix[2, 1], _matrix[2, 2], _matrix[2, 3], Environment.NewLine,
                _matrix[3, 0], _matrix[3, 1], _matrix[3, 2], _matrix[3, 3]);
        }

        #endregion
    }
}