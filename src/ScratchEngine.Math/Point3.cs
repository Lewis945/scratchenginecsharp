﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math.Interfaces;
using System;

namespace ScratchEngine.Math
{
    /// <summary>
    /// Struct represents a point in 3D space (Carthesian).
    /// </summary>
    public struct Point3 : IEquatable<Point3>, IAddition<Point3>, ISubtraction<Vector3, Point3>
    {
        #region Defaults

        /// <summary>
        /// Origin <see cref="Point3"/> point.
        /// </summary>
        public static Point3 Origin { get { return new Point3(0, 0, 0); } }

        #endregion

        #region Fields

        private readonly float _x;
        private readonly float _y;
        private readonly float _z;

        #endregion

        #region Properties

        /// <summary>
        /// x coordinate.
        /// </summary>
        public float X { get { return _x; } }

        /// <summary>
        /// y coordinate.
        /// </summary>
        public float Y { get { return _y; } }

        /// <summary>
        /// z coordinate.
        /// </summary>
        public float Z { get { return _z; } }

        #endregion

        #region .ctor

        /// <summary>
        /// Instantiates <see cref="Point3"/> struct with (x,y,z) coordinates.
        /// </summary>
        /// <param name="x">x coordinate.</param>
        /// <param name="y">y coordinate.</param>
        /// <param name="z">z coordinate.</param>
        public Point3(float x, float y, float z)
        {
            _x = x;
            _y = y;
            _z = z;
        }

        #endregion

        #region Static Methods

        /// <summary>
        /// Adds <see cref="Point3"/> p2 to <see cref="Point3"/> p1.
        /// </summary>
        /// <param name="p1">Point to add to.</param>
        /// <param name="p2">Point to add.</param>
        /// <returns>New point.</returns>
        public static Point3 Add(Point3 p1, Point3 p2)
        {
            return new Point3(p1.X + p2.X, p1.Y + p2.Y, p1.Z + p2.Z);
        }

        /// <summary>
        /// Adds <see cref="Vector3"/> v to <see cref="Point3"/> p.
        /// </summary>
        /// <param name="p">Point to add to.</param>
        /// <param name="v">Vector to add.</param>
        /// <returns>New point.</returns>
        public static Point3 Add(Point3 p, Vector3 v)
        {
            return new Point3(p.X + v.X, p.Y + v.Y, p.Z + v.Z);
        }

        /// <summary>
        /// Subtracts <see cref="Point3"/> p2 from <see cref="Point3"/> p1.
        /// </summary>
        /// <param name="p1">Point to substrat from.</param>
        /// <param name="p2">Point to subtract.</param>
        /// <returns>New point.</returns>
        public static Vector3 Subtract(Point3 p1, Point3 p2)
        {
            return new Vector3(p1.X - p2.X, p1.Y - p2.Y, p1.Z - p2.Z);
        }

        /// <summary>
        /// Subtracts <see cref="Vector3"/> v from <see cref="Point3"/> p.
        /// </summary>
        /// <param name="p">Point to subtract from.</param>
        /// <param name="v">Vector to subtract.</param>
        /// <returns>New point.</returns>
        public static Point3 Subtract(Point3 p, Vector3 v)
        {
            return new Point3(p.X - v.X, p.Y - v.Y, p.Z - v.Z);
        }

        /// <summary>
        /// Compare points (<see cref="Point3"/>) for equality.
        /// </summary>
        /// <param name="p1">Point to compare.</param>
        /// <param name="p2">Point comapre to.</param>
        /// <returns>New point.</returns>
        public static bool Equals(Point3 p1, Point3 p2)
        {
            return p1.Equals(p2);
        }

        #endregion

        #region Methods

        /// <summary>
        /// Adds <see cref="Point3"/> p to this <see cref="Point3"/> object.
        /// </summary>
        /// <param name="p">Point to add.</param>
        /// <returns>New point.</returns>
        public Point3 Add(Point3 p)
        {
            return Add(this, p);
        }

        /// <summary>
        /// Adds <see cref="Point3"/> v to this <see cref="Point3"/> object.
        /// </summary>
        /// <param name="v">Vector to add.</param>
        /// <returns>New point.</returns>
        public Point3 Add(Vector3 v)
        {
            return Add(this, v);
        }

        /// <summary>
        /// Subtracts <see cref="Point3"/> p from this <see cref="Point3"/> object.
        /// </summary>
        /// <param name="p">Point to subtract.</param>
        /// <returns>New vector.</returns>
        public Vector3 Subtract(Point3 p)
        {
            return Subtract(this, p);
        }

        /// <summary>
        /// Subtracts <see cref="Vector3"/> v from this <see cref="Point3"/> object.
        /// </summary>
        /// <param name="v">Vector to subtract.</param>
        /// <returns>New point.</returns>
        public Point3 Subtract(Vector3 v)
        {
            return Subtract(this, v);
        }

        /// <summary>
        /// Checks if this <see cref="Point3"/> object is equal to <see cref="Point3"/> p.
        /// </summary>
        /// <param name="p">Point compare to.</param>
        /// <returns>Boolean result of a check.</returns>
        public bool Equals(Point3 p)
        {
            if (p == null)
                return false;

            return (_x == p.X) && (_y == p.Y) && (_z == p.Z);
        }

        #endregion

        #region Operators

        /// <summary>
        /// Adds <see cref="Point3"/> p2 to <see cref="Point3"/> p1.
        /// </summary>
        /// <param name="p1">Point to add to.</param>
        /// <param name="p2">Point to add.</param>
        /// <returns>New point.</returns>
        public static Point3 operator +(Point3 p1, Point3 p2)
        {
            return Add(p1, p2);
        }

        /// <summary>
        /// Adds <see cref="Vector3"/> v to <see cref="Point3"/> p.
        /// </summary>
        /// <param name="p">Point to add to.</param>
        /// <param name="v">Vector to add.</param>
        /// <returns>New point.</returns>
        public static Point3 operator +(Point3 p, Vector3 v)
        {
            return Add(p, v);
        }

        /// <summary>
        /// Subtracts <see cref="Point3"/> p2 from <see cref="Point3"/> p1.
        /// </summary>
        /// <param name="p1">Point to substrat from.</param>
        /// <param name="p2">Point to subtract.</param>
        /// <returns>New point.</returns>
        public static Vector3 operator -(Point3 p1, Point3 p2)
        {
            return Subtract(p1, p2);
        }

        /// <summary>
        /// Subtracts <see cref="Vector3"/> v from <see cref="Point3"/> p.
        /// </summary>
        /// <param name="p">Point to subtract from.</param>
        /// <param name="v">Vector to subtract.</param>
        /// <returns>New point.</returns>
        public static Point3 operator -(Point3 p, Vector3 v)
        {
            return Subtract(p, v);
        }

        /// <summary>
        /// Checks if <see cref="Point3"/> p1 is equal to <see cref="Point3"/> p2.
        /// </summary>
        /// <param name="p1">Point compare to.</param>
        /// <param name="p2">Point to compare.</param>
        /// <returns>Boolean result of a check.</returns>
        public static bool operator ==(Point3 p1, Point3 p2)
        {
            return p1.Equals(p2);
        }

        /// <summary>
        /// Checks if <see cref="Point3"/> p1 is not equal to <see cref="Point3"/> p2.
        /// </summary>
        /// <param name="p1">Point compare to.</param>
        /// <param name="p2">Point to compare.</param>
        /// <returns>Boolean result of a check.</returns>
        public static bool operator !=(Point3 p1, Point3 p2)
        {
            return !p1.Equals(p2);
        }

        #endregion

        #region Override

        /// <summary>
        /// Checks if <see cref="Point3"/> object is equal to <see cref="object"/> obj.
        /// </summary>
        /// <param name="obj">Object compare to.</param>
        /// <returns>Boolean result of a check.</returns>
        public override bool Equals(object obj)
        {
            // If parameter cannot be cast to Point3 return false.
            if (!(obj is Point3))
                return false;

            // Return true if the fields match:
            return Equals((Point3)obj);
        }

        /// <summary>
        /// Computes hashcode based on (x,y,z) coordinates for <see cref="Point3"/>.
        /// </summary>
        /// <returns>Hashcode.</returns>
        public override int GetHashCode()
        {
            int hash = 13;
            hash = (hash * 7) + _x.GetHashCode();
            hash = (hash * 7) + _y.GetHashCode();
            hash = (hash * 7) + _z.GetHashCode();
            return hash;
        }

        /// <summary>
        /// Prints <see cref="Point3"/> as "Point3(x = {x}, y = {y}, z = {z})".
        /// </summary>
        /// <returns>String representation of the <see cref="Point3"/>.</returns>
        public override string ToString()
        {
            return string.Format("Point3(x = {0}, y = {1}, z = {2})", _x, _y, _z);
        }

        #endregion
    }
}