﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math.Exceptions;

namespace ScratchEngine.Math
{
    /// <summary>
    /// 
    /// </summary>
    public static class MatrixUtilities
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="length"></param>
        /// <param name="value"></param>
        /// <returns></returns>
        public static float[] GetEmpty(int length, float value = 0)
        {
            var matrix = new float[length];
            for (int i = 0; i < length; i++)
                matrix[i] = value;
            return matrix;
        }

        /// <summary>
        /// Returns <see cref="float[,]"/> empty matrix. 
        /// </summary>
        /// <param name="rows">Number of rows.</param>
        /// <param name="columns">Number of columns.</param>
        /// <param name="value"></param>
        /// <returns>New <see cref="float[,]"/> matrix.</returns>
        public static float[,] GetEmpty(int rows, int columns, float value = 0)
        {
            var matrix = new float[rows, columns];
            for (int i = 0; i < rows; i++)
                for (int j = 0; j < columns; j++)
                    matrix[i, j] = value;
            return matrix;
        }

        /// <summary>
        /// Returns <see cref="float[,]"/> identity matrix. 
        /// </summary>
        /// <param name="length">Number of rows and columns.</param>
        /// <returns>New <see cref="float[,]"/> matrix.</returns>
        public static float[,] GetIdentity(int length)
        {
            var matrix = new float[length, length];
            for (int i = 0; i < length; i++)
                for (int j = 0; j < length; j++)
                    if (i == j) matrix[i, j] = 1;
                    else matrix[i, j] = 0;
            return matrix;
        }

        /// <summary>
        /// Multiplies <see cref="float[,]"/> m1 with <see cref="float[,]"/> m2.
        /// </summary>
        /// <param name="m1">Matrix to multiply with.</param>
        /// <param name="m2">Matrix to multiply.</param>
        /// <returns>New <see cref="float[,]"/> matrix.</returns>
        public static float[,] Multiply(float[,] m1, float[,] m2)
        {
            int m1Rows = m1.GetLength(0);
            int m1Columns = m1.GetLength(1);
            int m2Rows = m2.GetLength(0);
            int m2Columns = m2.GetLength(1);

            if (m1Columns != m2Rows)
                throw new MathException("Number of columns in m1 should be equal to number of rows in m2.");

            var matrix = GetEmpty(m1Rows, m2Columns);
            for (int i = 0; i < m1Rows; i++)
                for (int j = 0; j < m2Columns; j++)
                    for (int k = 0; k < m1Columns; k++)
                        matrix[i, j] = matrix[i, j] + m1[i, k] * m2[k, j];
            return matrix;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m1"></param>
        /// <param name="m2"></param>
        /// <returns></returns>
        public static float[] Multiply(float[] m1, float[,] m2)
        {
            int m1Length = m1.GetLength(0);
            int m2Rows = m2.GetLength(0);
            int m2Columns = m2.GetLength(1);

            if (m1Length != m2Rows)
                throw new MathException("Length of 1-d array m1 should be equal to number of rows in 2-d array m2.");

            var matrix = GetEmpty(m1Length);
            for (int i = 0; i < m1Length; i++)
                for (int k = 0; k < m1Length; k++)
                    matrix[i] = matrix[i] + m1[k] * m2[k, i];

            return matrix;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m1"></param>
        /// <param name="m2"></param>
        /// <returns></returns>
        public static float[] Multiply(float[,] m1, float[] m2)
        {
            int m1Rows = m1.GetLength(0);
            int m1Columns = m1.GetLength(1);
            int m2Length = m2.GetLength(0);

            if (m1Columns != m2Length)
                throw new MathException("Length of 1-d array m2 should be equal to number of columns in 2-d array m1.");

            var matrix = GetEmpty(m2Length);
            for (int i = 0; i < m1Rows; i++)
                for (int k = 0; k < m1Columns; k++)
                    matrix[i] = matrix[i] + m1[i, k] * m2[k];

            return matrix;
        }

        /// <summary>
        /// Adds <see cref="float[,]"/> m2 to <see cref="float[,]"/> m1.
        /// </summary>
        /// <param name="m1">Matrix to add to.</param>
        /// <param name="m2">Matrix to add.</param>
        /// <returns>New <see cref="float[,]"/> matrix.</returns>
        public static float[,] Add(float[,] m1, float[,] m2)
        {
            int m1Rows = m1.GetLength(0);
            int m1Columns = m1.GetLength(1);
            int m2Rows = m2.GetLength(0);
            int m2Columns = m2.GetLength(1);

            if (m1Rows != m2Rows && m1Columns != m2Columns)
                throw new MathException("Number of columns and rows in tow matrices are not equal.");

            var matrix = new float[m1Rows, m1Columns];

            for (int i = 0; i < m1Rows; i++)
                for (int j = 0; j < m1Columns; j++)
                    matrix[i, j] = m1[i, j] + m2[i, j];

            return matrix;
        }

        /// <summary>
        /// Subtracts <see cref="float[,]"/> m2 from <see cref="float[,]"/> m1.
        /// </summary>
        /// <param name="m1">Matrix to subtract from.</param>
        /// <param name="m2">Matrix to subtract.</param>
        /// <returns>New <see cref="float[,]"/> matrix.</returns>
        public static float[,] Substract(float[,] m1, float[,] m2)
        {
            int m1Rows = m1.GetLength(0);
            int m1Columns = m1.GetLength(1);
            int m2Rows = m2.GetLength(0);
            int m2Columns = m2.GetLength(1);

            if (m1Rows != m2Rows && m1Columns != m2Columns)
                throw new MathException("Number of columns and rows in tow matrices are not equal.");

            var matrix = new float[m1Rows, m1Columns];

            for (int i = 0; i < m1Rows; i++)
                for (int j = 0; j < m1Columns; j++)
                    matrix[i, j] = m1[i, j] - m2[i, j];

            return matrix;
        }

        /// <summary>
        /// Transposes <see cref="float[,]"/> m
        /// </summary>
        /// <param name="m">Matrix to transpose</param>
        /// <returns>>New <see cref="float[,]"/> matrix.</returns>
        public static float[,] Transpose(float[,] m)
        {
            int rows = m.GetLength(0);
            int columns = m.GetLength(1);

            var matrix = new float[columns, rows];

            for (int i = 0; i < rows; i++)
                for (int j = 0; j < columns; j++)
                    matrix[j, i] = m[i, j];

            return matrix;
        }
    }
}