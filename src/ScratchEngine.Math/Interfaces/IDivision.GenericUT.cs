﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

namespace ScratchEngine.Math.Interfaces
{
    /// <summary>
    /// Defines a generalized method that a value type or class implements to create
    /// a type-specific method for determining division of instances.
    /// </summary>
    /// <typeparam name="U">The type of objects to return.</typeparam>
    /// <typeparam name="T">The type of objects to divide by.</typeparam>
    public interface IDivision<U, T>
    {
        /// <summary>
        /// Divides this object by <see cref="T"/> object.
        /// </summary>
        /// <param name="other">An object to divide by.</param>
        /// <returns>New object <see cref="U"/> that is product of division.</returns>
        U Divide(T other);
    }
}