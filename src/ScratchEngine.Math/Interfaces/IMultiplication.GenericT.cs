﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

namespace ScratchEngine.Math.Interfaces
{
    /// <summary>
    /// Defines a generalized method that a value type or class implements to create
    /// a type-specific method for determining multiplication of instances.
    /// </summary>
    /// <typeparam name="T">The type of objects to multiply.</typeparam>
    public interface IMultiplication<T>
    {
        /// <summary>
        /// Multiplies <see cref="T"/> object to this object.
        /// </summary>
        /// <param name="other">An object to multiply with this object.</param>
        /// <returns>New object that is product of multiplication.</returns>
        T Multiply(T other);
    }
}