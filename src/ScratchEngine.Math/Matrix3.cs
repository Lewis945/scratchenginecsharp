﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using ScratchEngine.Math.Interfaces;
using System;

namespace ScratchEngine.Math
{
    /// <summary>
    /// 
    /// </summary>
    public struct Matrix3 : IMultiplication<Matrix3>
    {
        #region Defaults

        /// <summary>
        /// Returns empty 3x3 matrix.
        /// </summary>
        public static Matrix3 Empty { get { return new Matrix3(MatrixUtilities.GetEmpty(GetLength(), GetLength())); } }

        /// <summary>
        /// Returns 3x3 identity matrix.
        /// </summary>
        public static Matrix3 Identity { get { return new Matrix3(MatrixUtilities.GetIdentity(GetLength())); } }

        #endregion

        #region Fields

        private float[,] _matrix;

        #endregion

        #region Indexer

        /// <summary>
        /// Gets/Sets float number at a position.
        /// </summary>
        /// <param name="i">Row index.</param>
        /// <param name="j">Column index.</param>
        /// <returns><see cref="float"/> number.</returns>
        public float this[int i, int j]
        {
            get
            {
                return _matrix[i, j];
            }
            set
            {
                _matrix[i, j] = value;
            }
        }

        #endregion

        #region Properties

        /// <summary>
        /// Length of the matrix in both dimensions.
        /// </summary>
        public int Length { get { return GetLength(); } }

        #endregion

        #region .ctor

        /// <summary>
        /// Instantiates <see cref="Matrix3"/> struct with float[3,3] array.
        /// </summary>
        /// <param name="matrix">float[3,3] array.</param>
        public Matrix3(float[,] matrix)
        {
            if (matrix.GetLength(0) != GetLength() || matrix.GetLength(1) != GetLength())
                throw new Exception("Matrix dimensions are not correct.");

            _matrix = matrix;
        }

        #endregion

        #region Static

        /// <summary>
        /// Returns length of the matrix in both dimensions.
        /// </summary>
        public static int GetLength() { return 3; }

        /// <summary>
        /// Multiplies <see cref="Matrix3"/> m1 with <see cref="Matrix3"/> m2.
        /// </summary>
        /// <param name="m1">Matrix to multiply with.</param>
        /// <param name="m2">Matrix to multiply.</param>
        /// <returns>New <see cref="Matrix3"/> matrix.</returns>
        public static Matrix3 Multiply(Matrix3 m1, Matrix3 m2)
        {
            return new Matrix3(MatrixUtilities.Multiply(m1.ToArray(), m2.ToArray()));
        }

        /// <summary>
        /// Adds <see cref="Matrix3"/> m2 to <see cref="Matrix3"/> m1.
        /// </summary>
        /// <param name="m1">Matrix to add to.</param>
        /// <param name="m2">Matrix to add.</param>
        /// <returns>New <see cref="Matrix3"/> matrix.</returns>
        public static Matrix3 Add(Matrix3 m1, Matrix3 m2)
        {
            return new Matrix3(MatrixUtilities.Add(m1.ToArray(), m2.ToArray()));
        }

        /// <summary>
        /// Subtracts <see cref="Matrix3"/> m2 from <see cref="Matrix3"/> m1.
        /// </summary>
        /// <param name="m1">Matrix to subtract from.</param>
        /// <param name="m2">Matrix to subtract.</param>
        /// <returns>New <see cref="Matrix3"/> matrix.</returns>
        public static Matrix3 Subtract(Matrix3 m1, Matrix3 m2)
        {
            return new Matrix3(MatrixUtilities.Substract(m1.ToArray(), m2.ToArray()));
        }

        /// <summary>
        /// Transposes <see cref="Matrix3"/> m
        /// </summary>
        /// <param name="m">Matrix to transpose</param>
        /// <returns>>New <see cref="Matrix3"/> matrix.</returns>
        public static Matrix3 Transpose(Matrix3 m)
        {
            return new Matrix3(MatrixUtilities.Transpose(m.ToArray()));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        public static float[,] ToArray(Matrix3 m)
        {
            return m._matrix;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Multiplies <see cref="Matrix3"/> matrix with this <see cref="Matrix3"/> object.
        /// </summary>
        /// <param name="matrix">Matrix to multiply.</param>
        /// <returns>New <see cref="Matrix3"/> matrix.</returns>
        public Matrix3 Multiply(Matrix3 matrix)
        {
            return Multiply(this, matrix);
        }

        /// <summary>
        /// Adds <see cref="Matrix3"/> matrix to this <see cref="Matrix3"/> object.
        /// </summary>
        /// <param name="matrix">Matrix to add.</param>
        /// <returns>New <see cref="Matrix3"/> matrix.</returns>
        public Matrix3 Add(Matrix3 matrix)
        {
            return Add(this, matrix);
        }

        /// <summary>
        /// Subtracts <see cref="Matrix3"/> matrix from this <see cref="Matrix3"/> object.
        /// </summary>
        /// <param name="matrix">Matrix to substract.</param>
        /// <returns>New <see cref="Matrix3"/> matrix.</returns>
        public Matrix3 Subtract(Matrix3 matrix)
        {
            return Subtract(this, matrix);
        }

        /// <summary>
        /// Transposes this <see cref="Matrix3"/> object.
        /// </summary>
        /// <returns>New <see cref="Matrix3"/> matrix.</returns>
        public Matrix3 Transpose()
        {
            return Transpose(this);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public float[,] ToArray()
        {
            return _matrix;
        }

        #endregion

        #region Operators

        /// <summary>
        /// Multiplies <see cref="Matrix3"/> m1 with <see cref="Matrix3"/> m2.
        /// </summary>
        /// <param name="m1">Matrix multiply with.</param>
        /// <param name="m2">Matrix to multiply.</param>
        /// <returns>New <see cref="Matrix3"/> matrix.</returns>
        public static Matrix3 operator *(Matrix3 m1, Matrix3 m2)
        {
            return Multiply(m1, m2);
        }

        /// <summary>
        /// Adds <see cref="Matrix3"/> m2 to <see cref="Matrix3"/> m1.
        /// </summary>
        /// <param name="m1">Matrix to add to.</param>
        /// <param name="m2">Matrix to add.</param>
        /// <returns>New <see cref="Matrix3"/> matrix.</returns>
        public static Matrix3 operator +(Matrix3 m1, Matrix3 m2)
        {
            return Add(m1, m2);
        }

        /// <summary>
        /// Subtracts <see cref="Matrix3"/> m2 from <see cref="Matrix3"/> m1.
        /// </summary>
        /// <param name="m1">Matrix to substract from.</param>
        /// <param name="m2">Matrix to substract.</param>
        /// <returns>New <see cref="Matrix3"/> matrix.</returns>
        public static Matrix3 operator -(Matrix3 m1, Matrix3 m2)
        {
            return Subtract(m1, m2);
        }

        #endregion

        #region Override

        /// <summary>
        /// Prints <see cref="Matrix3"/> as "Matrix3 [ m11 m12 m13 ]".
        ///                                          [ m21 m22 m23 ]
        ///                                          [ m31 m32 m33 ]
        /// </summary>
        /// <returns>String representation of the <see cref="Matrix3"/>.</returns>
        public override string ToString()
        {
            return string.Format("Matrix4 [ {0} {1} {2} ] {3} [ {4} {5} {6} ] {7} [ {8} {9} {10} ]",
                _matrix[0, 0], _matrix[0, 1], _matrix[0, 2], Environment.NewLine,
                _matrix[1, 0], _matrix[1, 1], _matrix[1, 2], Environment.NewLine,
                _matrix[2, 0], _matrix[2, 1], _matrix[2, 2]);
        }

        #endregion
    }
}