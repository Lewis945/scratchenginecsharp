﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using NUnit.Framework;
using ScratchEngine.Math;

namespace ScratchEngine.Core.Tests
{
    [TestFixture]
    public class MatrixUtilitiesTests
    {
        #region Fields

        #endregion

        #region .ctor

        public MatrixUtilitiesTests()
        {
        }

        #endregion

        #region SetUp

        [SetUp]
        public void Init()
        {
        }

        [TearDown]
        public void Cleanup()
        {
        }

        #endregion

        #region Static Methods

        [Test]
        public void Empty_Matrix()
        {
            int rows = 4;
            int columns = 4;

            var matrix = MatrixUtilities.GetEmpty(rows, columns);

            for (int i = 0; i < rows; i++)
                for (int j = 0; j < columns; j++)
                    Assert.AreEqual(0, matrix[i, j]);
        }

        [Test]
        public void Identity_Matrix()
        {
            int length = 4;

            var matrix = MatrixUtilities.GetIdentity(length);

            for (int i = 0; i < length; i++)
                for (int j = 0; j < length; j++)
                    if (i == j)
                        Assert.AreEqual(1, matrix[i, j]);
                    else
                        Assert.AreEqual(0, matrix[i, j]);
        }

        [Test]
        public void Matrix_2d_Multiplies_With_Matrix_1d()
        {
            var matrix2d = new float[,] {
                { 1, -1, 2 },
                { 0, -3, 1 },
                { 0,  0, 0 }
            };

            var matrix1d = new float[] { 2, 1, 0 };

            var result = MatrixUtilities.Multiply(matrix2d, matrix1d);

            Assert.That(result[0], Is.EqualTo(1));
            Assert.That(result[1], Is.EqualTo(-3));
            Assert.That(result[2], Is.EqualTo(0));
        }

        #endregion
    }
}