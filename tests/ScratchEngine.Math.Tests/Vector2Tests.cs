﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using NUnit.Framework;
using System;

namespace ScratchEngine.Math.Tests.Math
{
    [TestFixture]
    public class Vector2Tests
    {
        #region Fields

        private Vector2 _vector1;
        private Vector2 _vector2;
        private Vector2 _normal;
        private float _scalar;

        private Vector2 _additionVectorsExpected;
        private Vector2 _additionVector1ScalarExpected;
        private Vector2 _additionVector2ScalarExpected;

        private float _distanceVectorsExpected;

        private Vector2 _divisionVectorsExpected;
        private Vector2 _divisionVector1ScalarExpected;
        private Vector2 _divisionVector2ScalarExpected;

        private float _dotProductVectorsExpected;
        private float _lengthVector1Expected;
        private float _lengthVector2Expected;

        private Vector2 _multiplicationVectorsExpected;
        private Vector2 _multiplicationVector1ScalarExpected;
        private Vector2 _multiplicationVector2ScalarExpected;

        private Vector2 _negateVector1Expected;
        private Vector2 _negateVector2Expected;

        private Vector2 _normilizeVector1Expected;
        private Vector2 _normilizeVector2Expected;

        private Vector2 _reflectVector1Expected;
        private Vector2 _reflectVector2Expected;

        private Vector2 _subtractionVectorsExpected;
        private Vector2 _subtractionVector1ScalarExpected;
        private Vector2 _subtractionVector2ScalarExpected;

        private float[] _vector1ToArrayExpected;
        private float[] _vector2ToArrayExpected;

        private Point2 _vector1ToPoint2Expected;
        private Point2 _vector2ToPoint2Expected;

        private Vector3 _vector1ToVector3Expected;
        private Vector3 _vector2ToVector3Expected;

        #endregion

        #region .ctor

        public Vector2Tests()
        {
            _vector1 = new Vector2(5, 2);
            _vector2 = new Vector2(1, 4);
            _normal = new Vector2(0, 1);
            _scalar = 0.4f;

            _additionVectorsExpected = new Vector2(6, 6);
            _additionVector1ScalarExpected = new Vector2(5.4f, 2.4f);
            _additionVector2ScalarExpected = new Vector2(1.4f, 4.4f);

            _distanceVectorsExpected = 4.472135954999579f;

            _divisionVectorsExpected = new Vector2(5, 0.5f);
            _divisionVector1ScalarExpected = new Vector2(12.5f, 5);
            _divisionVector2ScalarExpected = new Vector2(2.5f, 10);

            _dotProductVectorsExpected = 13;
            _lengthVector1Expected = 5.385164807134504f;
            _lengthVector2Expected = 4.123105625617661f;

            _multiplicationVectorsExpected = new Vector2(5, 8);
            _multiplicationVector1ScalarExpected = new Vector2(2, 0.8f);
            _multiplicationVector2ScalarExpected = new Vector2(0.4f, 1.6f);

            _negateVector1Expected = new Vector2(-5, -2);
            _negateVector2Expected = new Vector2(-1, -4);

            _normilizeVector1Expected = new Vector2(0.9284766908852593f, 0.3713906763541037f);
            _normilizeVector2Expected = new Vector2(0.242535636f, 0.970142543f);

            _reflectVector1Expected = new Vector2(-5, 2);
            _reflectVector2Expected = new Vector2(-1, 4);

            _subtractionVectorsExpected = new Vector2(4, -2);
            _subtractionVector1ScalarExpected = new Vector2(4.6f, 1.6f);
            _subtractionVector2ScalarExpected = new Vector2(0.6f, 3.6f);

            _vector1ToArrayExpected = new float[] { 5, 2 };
            _vector2ToArrayExpected = new float[] { 1, 4 };

            _vector1ToPoint2Expected = new Point2(5, 2);
            _vector2ToPoint2Expected = new Point2(1, 4);

            _vector1ToVector3Expected = new Vector3(5, 2, 0);
            _vector2ToVector3Expected = new Vector3(1, 4, 0);
        }

        #endregion

        #region SetUp

        [SetUp]
        public void Init()
        {
        }

        [TearDown]
        public void Cleanup()
        {
        }

        #endregion

        #region Operations

        [Test]
        public void Vector_Addition()
        {
            #region Vector1 + Vector2

            Vector2 resultStatic = Vector2.Add(_vector1, _vector2);
            Vector2 resultMethod = _vector1.Add(_vector2);
            Vector2 resultOperator = _vector1 + _vector2;

            Assert.That(resultStatic, Is.EqualTo(_additionVectorsExpected));
            Assert.That(resultMethod, Is.EqualTo(_additionVectorsExpected));
            Assert.That(resultOperator, Is.EqualTo(_additionVectorsExpected));

            #endregion

            #region Vector1 + Scalar

            Vector2 resultVector1ScalarStatic = Vector2.Add(_vector1, _scalar);
            Vector2 resultVector1ScalarMethod = _vector1.Add(_scalar);
            Vector2 resultVector1ScalarOperator = _vector1 + _scalar;
            Vector2 resultScalarVector1Operator = _scalar + _vector1;

            Assert.That(resultVector1ScalarStatic, Is.EqualTo(_additionVector1ScalarExpected));
            Assert.That(resultVector1ScalarMethod, Is.EqualTo(_additionVector1ScalarExpected));
            Assert.That(resultVector1ScalarOperator, Is.EqualTo(_additionVector1ScalarExpected));
            Assert.That(resultScalarVector1Operator, Is.EqualTo(_additionVector1ScalarExpected));

            #endregion

            #region Vector2 + Scalar

            Vector2 resultVector2ScalarStatic = Vector2.Add(_vector2, _scalar);
            Vector2 resultVector2ScalarMethod = _vector2.Add(_scalar);
            Vector2 resultVector2ScalarOperator = _vector2 + _scalar;
            Vector2 resultScalarVector2Operator = _scalar + _vector2;

            Assert.That(resultVector2ScalarStatic, Is.EqualTo(_additionVector2ScalarExpected));
            Assert.That(resultVector2ScalarMethod, Is.EqualTo(_additionVector2ScalarExpected));
            Assert.That(resultVector2ScalarOperator, Is.EqualTo(_additionVector2ScalarExpected));
            Assert.That(resultScalarVector2Operator, Is.EqualTo(_additionVector2ScalarExpected));

            #endregion
        }

        [Test]
        public void Vector_Distance()
        {
            #region Vector1 + Vector2

            float resultStatic = Vector2.Distance(_vector1, _vector2);
            float resultMethod = _vector1.Distance(_vector2);

            Assert.That(resultStatic, Is.EqualTo(_distanceVectorsExpected));
            Assert.That(resultMethod, Is.EqualTo(_distanceVectorsExpected));

            #endregion
        }

        [Test]
        public void Vector_Division()
        {
            #region Vector1 / Vector2

            Vector2 resultStatic = Vector2.Divide(_vector1, _vector2);
            Vector2 resultMethod = _vector1.Divide(_vector2);
            Vector2 resultOperator = _vector1 / _vector2;

            Assert.That(resultStatic, Is.EqualTo(_divisionVectorsExpected));
            Assert.That(resultMethod, Is.EqualTo(_divisionVectorsExpected));
            Assert.That(resultOperator, Is.EqualTo(_divisionVectorsExpected));

            #endregion

            #region Vector1 / Scalar

            Vector2 resultVector1ScalarStatic = Vector2.Divide(_vector1, _scalar);
            Vector2 resultVector1ScalarMethod = _vector1.Divide(_scalar);
            Vector2 resultVector1ScalarOperator = _vector1 / _scalar;

            Assert.That(resultVector1ScalarStatic, Is.EqualTo(_divisionVector1ScalarExpected));
            Assert.That(resultVector1ScalarMethod, Is.EqualTo(_divisionVector1ScalarExpected));
            Assert.That(resultVector1ScalarOperator, Is.EqualTo(_divisionVector1ScalarExpected));

            #endregion

            #region Vector2 / Scalar

            Vector2 resultVector2ScalarStatic = Vector2.Divide(_vector2, _scalar);
            Vector2 resultVector2ScalarMethod = _vector2.Divide(_scalar);
            Vector2 resultVector2ScalarOperator = _vector2 / _scalar;

            Assert.That(resultVector2ScalarStatic, Is.EqualTo(_divisionVector2ScalarExpected));
            Assert.That(resultVector2ScalarMethod, Is.EqualTo(_divisionVector2ScalarExpected));
            Assert.That(resultVector2ScalarOperator, Is.EqualTo(_divisionVector2ScalarExpected));

            #endregion
        }

        [Test]
        public void Vector_DotProduct()
        {
            #region Vector1 dot Vector2

            float resultStatic = Vector2.DotProduct(_vector1, _vector2);
            float resultMethod = _vector1.DotProduct(_vector2);

            Assert.That(resultStatic, Is.EqualTo(_dotProductVectorsExpected));
            Assert.That(resultMethod, Is.EqualTo(_dotProductVectorsExpected));

            #endregion
        }

        [Test]
        public void Vector_Equals()
        {
            #region Vector1 Equals Vector1

            var vector1Copy = _vector1;

            bool resultStatic = Vector2.Equals(_vector1, vector1Copy);
            bool resultMethod = _vector1.Equals(vector1Copy);
            bool resultOperator = _vector1 == vector1Copy;

            Assert.That(resultStatic, Is.EqualTo(true));
            Assert.That(resultMethod, Is.EqualTo(true));
            Assert.That(resultOperator, Is.EqualTo(true));

            #endregion

            #region Vector2 Equals Vector2

            var vector2Copy = _vector2;

            resultStatic = Vector2.Equals(_vector2, vector2Copy);
            resultMethod = _vector2.Equals(vector2Copy);
            resultOperator = _vector2 == vector2Copy;

            Assert.That(resultStatic, Is.EqualTo(true));
            Assert.That(resultMethod, Is.EqualTo(true));
            Assert.That(resultOperator, Is.EqualTo(true));

            #endregion

            #region Vector1 Equals Vector2

            resultStatic = Vector2.Equals(_vector1, _vector2);
            resultMethod = _vector1.Equals(_vector2);
            resultOperator = _vector1 == _vector2;

            Assert.That(resultStatic, Is.EqualTo(false));
            Assert.That(resultMethod, Is.EqualTo(false));
            Assert.That(resultOperator, Is.EqualTo(false));

            #endregion

            #region Vector2 Equals Vector1

            resultStatic = Vector2.Equals(_vector2, _vector1);
            resultMethod = _vector2.Equals(_vector1);
            resultOperator = _vector2 == _vector1;

            Assert.That(resultStatic, Is.EqualTo(false));
            Assert.That(resultMethod, Is.EqualTo(false));
            Assert.That(resultOperator, Is.EqualTo(false));

            #endregion

            #region Vector1 NotEquals Vector2

            resultStatic = !Vector2.Equals(_vector1, _vector2);
            resultMethod = !_vector1.Equals(_vector2);
            resultOperator = _vector1 != _vector2;

            Assert.That(resultStatic, Is.EqualTo(true));
            Assert.That(resultMethod, Is.EqualTo(true));
            Assert.That(resultOperator, Is.EqualTo(true));

            #endregion

            #region Vector2 NotEquals Vector1

            resultStatic = !Vector2.Equals(_vector2, _vector1);
            resultMethod = !_vector2.Equals(_vector1);
            resultOperator = _vector2 != _vector1;

            Assert.That(resultStatic, Is.EqualTo(true));
            Assert.That(resultMethod, Is.EqualTo(true));
            Assert.That(resultOperator, Is.EqualTo(true));

            #endregion
        }

        [Test]
        public void Vector_Length()
        {
            #region Vector1

            float resultStatic = Vector2.GetLength(_vector1);
            float resultMethod = _vector1.Length;

            Assert.That(resultStatic, Is.EqualTo(_lengthVector1Expected));
            Assert.That(resultMethod, Is.EqualTo(_lengthVector1Expected));

            #endregion

            #region Vector2

            resultStatic = Vector2.GetLength(_vector2);
            resultMethod = _vector2.Length;

            Assert.That(resultStatic, Is.EqualTo(_lengthVector2Expected));
            Assert.That(resultMethod, Is.EqualTo(_lengthVector2Expected));

            #endregion
        }

        [Test]
        public void Vector_Multiplication()
        {
            #region Vector1 * Vector2

            Vector2 resultStatic = Vector2.Multiply(_vector1, _vector2);
            Vector2 resultMethod = _vector1.Multiply(_vector2);
            Vector2 resultOperator = _vector1 * _vector2;

            Assert.That(resultStatic, Is.EqualTo(_multiplicationVectorsExpected));
            Assert.That(resultMethod, Is.EqualTo(_multiplicationVectorsExpected));
            Assert.That(resultOperator, Is.EqualTo(_multiplicationVectorsExpected));

            #endregion

            #region Vector1 * Scalar

            Vector2 resultVector1ScalarStatic = Vector2.Multiply(_vector1, _scalar);
            Vector2 resultVector1SalarMethod = _vector1.Multiply(_scalar);
            Vector2 resultVector1ScalarOperator = _vector1 * _scalar;
            Vector2 resultScalarVector1Operator = _scalar * _vector1;

            Assert.That(resultVector1ScalarStatic, Is.EqualTo(_multiplicationVector1ScalarExpected));
            Assert.That(resultVector1SalarMethod, Is.EqualTo(_multiplicationVector1ScalarExpected));
            Assert.That(resultVector1ScalarOperator, Is.EqualTo(_multiplicationVector1ScalarExpected));
            Assert.That(resultScalarVector1Operator, Is.EqualTo(_multiplicationVector1ScalarExpected));

            #endregion

            #region Vector2 * Scalar

            Vector2 resultVector2ScalarStatic = Vector2.Multiply(_vector2, _scalar);
            Vector2 resultVector2ScalarMethod = _vector2.Multiply(_scalar);
            Vector2 resultVector2ScalarOperator = _vector2 * _scalar;
            Vector2 resultScalarVector2Operator = _scalar * _vector2;

            Assert.That(resultVector2ScalarStatic, Is.EqualTo(_multiplicationVector2ScalarExpected));
            Assert.That(resultVector2ScalarMethod, Is.EqualTo(_multiplicationVector2ScalarExpected));
            Assert.That(resultVector2ScalarOperator, Is.EqualTo(_multiplicationVector2ScalarExpected));
            Assert.That(resultScalarVector2Operator, Is.EqualTo(_multiplicationVector2ScalarExpected));

            #endregion
        }

        [Test]
        public void Vector_Negate()
        {
            #region Vector1

            Vector2 resultStatic = Vector2.Negate(_vector1);
            Vector2 resultMethod = _vector1.Negate();
            Vector2 resultOperator = -_vector1;

            Assert.That(resultStatic, Is.EqualTo(_negateVector1Expected));
            Assert.That(resultMethod, Is.EqualTo(_negateVector1Expected));
            Assert.That(resultOperator, Is.EqualTo(_negateVector1Expected));

            #endregion

            #region Vector2

            resultStatic = Vector2.Negate(_vector2);
            resultMethod = _vector2.Negate();
            resultOperator = -_vector2;

            Assert.That(resultStatic, Is.EqualTo(_negateVector2Expected));
            Assert.That(resultMethod, Is.EqualTo(_negateVector2Expected));
            Assert.That(resultOperator, Is.EqualTo(_negateVector2Expected));

            #endregion
        }

        [Test]
        public void Vector_Normilize()
        {
            #region Vector1

            Vector2 resultStatic = Vector2.Normalize(_vector1);
            Vector2 resultMethod = _vector1.Normalize();

            Assert.That(resultStatic, Is.EqualTo(_normilizeVector1Expected));
            Assert.That(resultMethod, Is.EqualTo(_normilizeVector1Expected));

            #endregion

            #region Vector2

            resultStatic = Vector2.Normalize(_vector2);
            resultMethod = _vector2.Normalize();

            Assert.That(resultStatic, Is.EqualTo(_normilizeVector2Expected));
            Assert.That(resultMethod, Is.EqualTo(_normilizeVector2Expected));

            #endregion
        }

        [Test]
        public void Vector_Reflect()
        {
            #region Vector1

            Vector2 resultStatic = Vector2.Reflect(_vector1, _normal);
            Vector2 resultMethod = _vector1.Reflect(_normal);

            Assert.That(resultStatic, Is.EqualTo(_reflectVector1Expected));
            Assert.That(resultMethod, Is.EqualTo(_reflectVector1Expected));

            #endregion

            #region Vector2

            resultStatic = Vector2.Reflect(_vector2, _normal);
            resultMethod = _vector2.Reflect(_normal);

            Assert.That(resultStatic, Is.EqualTo(_reflectVector2Expected));
            Assert.That(resultMethod, Is.EqualTo(_reflectVector2Expected));

            #endregion
        }

        [Test]
        public void Vector_Subtraction()
        {
            #region Vector1 - Vector2

            Vector2 resultStatic = Vector2.Subtract(_vector1, _vector2);
            Vector2 resultMethod = _vector1.Subtract(_vector2);
            Vector2 resultOperator = _vector1 - _vector2;

            Assert.That(resultStatic, Is.EqualTo(_subtractionVectorsExpected));
            Assert.That(resultMethod, Is.EqualTo(_subtractionVectorsExpected));
            Assert.That(resultOperator, Is.EqualTo(_subtractionVectorsExpected));

            #endregion

            #region Vector1 - Scalar

            Vector2 resultVector1ScalarStatic = Vector2.Subtract(_vector1, _scalar);
            Vector2 resultVector1SalarMethod = _vector1.Subtract(_scalar);
            Vector2 resultVector1ScalarOperator = _vector1 - _scalar;
            Vector2 resultScalarVector1Operator = _scalar - _vector1;

            Assert.That(resultVector1ScalarStatic, Is.EqualTo(_subtractionVector1ScalarExpected));
            Assert.That(resultVector1SalarMethod, Is.EqualTo(_subtractionVector1ScalarExpected));
            Assert.That(resultVector1ScalarOperator, Is.EqualTo(_subtractionVector1ScalarExpected));
            Assert.That(resultScalarVector1Operator, Is.EqualTo(-_subtractionVector1ScalarExpected));

            #endregion

            #region Vector2 - Scalar

            Vector2 resultVector2ScalarStatic = Vector2.Subtract(_vector2, _scalar);
            Vector2 resultVector2ScalarMethod = _vector2.Subtract(_scalar);
            Vector2 resultVector2ScalarOperator = _vector2 - _scalar;
            Vector2 resultScalarVector2Operator = _scalar - _vector2;

            Assert.That(resultVector2ScalarStatic, Is.EqualTo(_subtractionVector2ScalarExpected));
            Assert.That(resultVector2ScalarMethod, Is.EqualTo(_subtractionVector2ScalarExpected));
            Assert.That(resultVector2ScalarOperator, Is.EqualTo(_subtractionVector2ScalarExpected));
            Assert.That(resultScalarVector2Operator, Is.EqualTo(-_subtractionVector2ScalarExpected));

            #endregion
        }

        [Test]
        public void Vector_Cast_To()
        {
            #region ToArray

            float[] resultArrayStatic = Vector2.ToArray(_vector1);
            float[] resultArrayMethod = _vector1.ToArray();
            float[] resultArrayCast = (float[])_vector1;

            Assert.That(resultArrayStatic, Is.EqualTo(_vector1ToArrayExpected));
            Assert.That(resultArrayMethod, Is.EqualTo(_vector1ToArrayExpected));
            Assert.That(resultArrayCast, Is.EqualTo(_vector1ToArrayExpected));

            resultArrayStatic = Vector2.ToArray(_vector2);
            resultArrayMethod = _vector2.ToArray();
            resultArrayCast = (float[])_vector2;

            Assert.That(resultArrayStatic, Is.EqualTo(_vector2ToArrayExpected));
            Assert.That(resultArrayMethod, Is.EqualTo(_vector2ToArrayExpected));
            Assert.That(resultArrayCast, Is.EqualTo(_vector2ToArrayExpected));

            #endregion

            #region ToPoint2

            var resultPoint2Static = Vector2.ToPoint2(_vector1);
            var resultPoint2Method = _vector1.ToPoint2();
            var resultPoint2Cast = (Point2)_vector1;

            Assert.That(resultPoint2Static, Is.EqualTo(_vector1ToPoint2Expected));
            Assert.That(resultPoint2Method, Is.EqualTo(_vector1ToPoint2Expected));
            Assert.That(resultPoint2Cast, Is.EqualTo(_vector1ToPoint2Expected));

            resultPoint2Static = Vector2.ToPoint2(_vector2);
            resultPoint2Method = _vector2.ToPoint2();
            resultPoint2Cast = (Point2)_vector2;

            Assert.That(resultPoint2Static, Is.EqualTo(_vector2ToPoint2Expected));
            Assert.That(resultPoint2Method, Is.EqualTo(_vector2ToPoint2Expected));
            Assert.That(resultPoint2Cast, Is.EqualTo(_vector2ToPoint2Expected));

            #endregion

            #region ToVector3

            var resultVector3Static = Vector2.ToVector3(_vector1);
            var resultVector3Method = _vector1.ToVector3();
            var resultVector3Cast = (Vector3)_vector1;

            Assert.That(resultVector3Static, Is.EqualTo(_vector1ToVector3Expected));
            Assert.That(resultVector3Method, Is.EqualTo(_vector1ToVector3Expected));
            Assert.That(resultVector3Cast, Is.EqualTo(_vector1ToVector3Expected));

            resultVector3Static = Vector2.ToVector3(_vector2);
            resultVector3Method = _vector2.ToVector3();
            resultVector3Cast = (Vector3)_vector2;

            Assert.That(resultVector3Static, Is.EqualTo(_vector2ToVector3Expected));
            Assert.That(resultVector3Method, Is.EqualTo(_vector2ToVector3Expected));
            Assert.That(resultVector3Cast, Is.EqualTo(_vector2ToVector3Expected));

            #endregion
        }

        #endregion
    }
}