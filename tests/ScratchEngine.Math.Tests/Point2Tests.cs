﻿#region LICENSE

//  Copyright 2016 Vitalii Zakharov (Lewis945)
//
//  Licensed under the Apache License, Version 2.0 (the "License"); you may not use this
//  file except in compliance with the License. You may obtain a copy of the License at
//
//    http://www.apache.org/licenses/LICENSE-2.0
//
//  Unless required by applicable law or agreed to in writing, software distributed under the License is 
//  distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. 
//  See the License for the specific language governing permissions and limitations under the License.

#endregion

using NUnit.Framework;
using ScratchEngine.Math;

namespace ScratchEngine.Math.Tests.Math
{
    [TestFixture]
    public class Point2Tests
    {
        #region Fields

        private Point2 _point;
        private Point2 _point2;
        private Vector2 _vector;

        private Point2 _additionPointsExpected;
        private Point2 _additionPointVectorExpected;
        private Point2 _subtractionPointVectorExpected;
        private Vector2 _subtractionPointsExpected;

        #endregion

        #region .ctor

        public Point2Tests()
        {
            _point = new Point2(5, 2);
            _point2 = new Point2(1, 4);
            _vector = new Vector2(2, 3);

            _additionPointsExpected = new Point2(6, 6);
            _additionPointVectorExpected = new Point2(7, 5);
            _subtractionPointVectorExpected = new Point2(3, -1);
            _subtractionPointsExpected = new Vector2(-4, 2);
        }

        #endregion

        #region SetUp

        [SetUp]
        public void Init()
        {
        }

        [TearDown]
        public void Cleanup()
        {
        }

        #endregion

        #region Operators

        [Test]
        public void Points_Addition_Operator()
        {
            var result = _point + _point2;
            Assert.AreEqual(_additionPointsExpected, result);
        }

        [Test]
        public void Points_Subtraction_Operator()
        {
            var result = _point2 - _point;
            Assert.AreEqual(_subtractionPointsExpected, result);
        }

        [Test]
        public void Point_Vector_Addition_Operator()
        {
            var result = _point + _vector;
            Assert.AreEqual(_additionPointVectorExpected, result);
        }

        [Test]
        public void Point_Vector_Subtraction_Operator()
        {
            var result = _point - _vector;
            Assert.AreEqual(_subtractionPointVectorExpected, result);
        }

        [Test]
        public void Points_Equality_Operator()
        {
            var p = _point;
            var result = _point == p;
            Assert.AreEqual(true, result);
        }

        [Test]
        public void Points_UnEquality_Operator()
        {
            var result = _point != _point2;
            Assert.AreEqual(true, result);
        }

        #endregion

        #region Static Methods

        [Test]
        public void Points_Addition_Static_Method()
        {
            var result = Point2.Add(_point, _point2);
            Assert.AreEqual(_additionPointsExpected, result);
        }

        [Test]
        public void Points_Subtraction_Static_Method()
        {
            var result = Point2.Subtract(_point2, _point);
            Assert.AreEqual(_subtractionPointsExpected, result);
        }

        [Test]
        public void Point_Vector_Addition_Static_Method()
        {
            var result = Point2.Add(_point, _vector);
            Assert.AreEqual(_additionPointVectorExpected, result);
        }

        [Test]
        public void Point_Vector_Subtraction_Static_Method()
        {
            var result = Point2.Subtract(_point, _vector);
            Assert.AreEqual(_subtractionPointVectorExpected, result);
        }

        [Test]
        public void Points_Equality_Static_Method()
        {
            var p = _point;
            var result = Point2.Equals(_point, p);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void Points_UnEquality_Static_Method()
        {
            var result = !Point2.Equals(_point, _point2);
            Assert.AreEqual(true, result);
        }

        #endregion

        #region Methods

        [Test]
        public void Points_Addition_Method()
        {
            var result = _point.Add(_point2);
            Assert.AreEqual(_additionPointsExpected, result);
        }

        [Test]
        public void Points_Subtraction_Method()
        {
            var result = _point2.Subtract(_point);
            Assert.AreEqual(_subtractionPointsExpected, result);
        }

        [Test]
        public void Point_Vector_Addition_Method()
        {
            var result = _point.Add(_vector);
            Assert.AreEqual(_additionPointVectorExpected, result);
        }

        [Test]
        public void Point_Vector_Subtraction_Method()
        {
            var result = _point.Subtract(_vector);
            Assert.AreEqual(_subtractionPointVectorExpected, result);
        }

        [Test]
        public void Points_Equality_Method()
        {
            var p = _point;
            var result = _point.Equals(p);
            Assert.AreEqual(true, result);
        }

        [Test]
        public void Points_UnEquality_Method()
        {
            var result = !_point.Equals(_point2);
            Assert.AreEqual(true, result);
        }

        #endregion
    }
}