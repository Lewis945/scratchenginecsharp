﻿using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SoftwareRendering
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPageWithSettings : Page
    {
        private SampleApplication _app;

        public void Page_Loaded(object sender, RoutedEventArgs e)
        {
            _app = new SampleApplication(frontBufferImageControl);

            // Registering to the XAML rendering loop
            CompositionTarget.Rendering += CompositionTarget_Rendering;
        }

        // Rendering loop handler
        private void CompositionTarget_Rendering(object sender, object e)
        {
            _app.Render();
        }

        public MainPageWithSettings()
        {
            InitializeComponent();
        }
    }
}