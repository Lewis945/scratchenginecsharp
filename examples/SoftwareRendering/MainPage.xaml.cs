﻿using System;
using System.Collections.ObjectModel;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace SoftwareRendering
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private SampleApplication _app;

        public void Page_Loaded(object sender, RoutedEventArgs e)
        {
            _app = new SampleApplication(frontBufferImageControl);

            // Registering to the XAML rendering loop
            CompositionTarget.Rendering += CompositionTarget_Rendering;

            CoreWindow.GetForCurrentThread().KeyDown += MainPage_KeyDown;
        }

        DateTime previousDate;
        private Collection<double> lastFPSValues = new Collection<double>();

        // Rendering loop handler
        private void CompositionTarget_Rendering(object sender, object e)
        {
            // Fps
            var now = DateTime.Now;
            var currentFps = 1000.0 / (now - previousDate).TotalMilliseconds;
            previousDate = now;

            fps.Text = string.Format("FPS: {0:0.00}", currentFps);

            if (lastFPSValues.Count < 60)
            {
                lastFPSValues.Add(currentFps);
            }
            else
            {
                lastFPSValues.RemoveAt(0);
                lastFPSValues.Add(currentFps);
                var totalValues = 0d;
                for (var i = 0; i < lastFPSValues.Count; i++)
                {
                    totalValues += lastFPSValues[i];
                }

                var averageFPS = totalValues / lastFPSValues.Count;
                averageFps.Text = string.Format("Average FPS: {0:0.00}", averageFPS);
            }

            _app.Update();
            _app.Render();
        }

        public MainPage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.  The Parameter
        /// property is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
        }

        protected override void OnPointerMoved(PointerRoutedEventArgs e)
        {
            var point = e.GetCurrentPoint(this);
            _app.InputMouse((float)point.Position.X, (float)point.Position.Y);
        }

        private void MainPage_KeyDown(CoreWindow sender, KeyEventArgs args)
        {
            _app.InputKey((int)args.VirtualKey);
        }
    }
}