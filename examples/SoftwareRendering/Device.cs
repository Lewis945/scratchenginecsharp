﻿using ScratchEngine.Math;
using ScratchEngine.Rendering;
using System;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.UI.Xaml.Media.Imaging;

namespace SoftwareRendering
{
    public class Device
    {
        private byte[] _backBuffer;
        private readonly float[] _depthBuffer;

        private object[] _lockBuffer;

        private WriteableBitmap _bmp;

        private readonly int _renderWidth;
        private readonly int _renderHeight;

        public Device(WriteableBitmap bmp)
        {
            _bmp = bmp;

            _renderWidth = bmp.PixelWidth;
            _renderHeight = bmp.PixelHeight;

            // the back buffer size is equal to the number of pixels to draw
            // on screen (width*height) * 4 (R,G,B & Alpha values). 
            _backBuffer = new byte[_renderWidth * _renderHeight * 4];
            _depthBuffer = new float[_renderWidth * _renderHeight];

            _lockBuffer = new object[_renderWidth * _renderHeight];
            for (var i = 0; i < _lockBuffer.Length; i++)
            {
                _lockBuffer[i] = new object();
            }
        }

        // This method is called to clear the back buffer with a specific color
        public void Clear(byte r, byte g, byte b, byte a)
        {
            // Clearing Back Buffer
            for (var index = 0; index < _backBuffer.Length; index += 4)
            {
                // BGRA is used by Windows instead by RGBA in HTML5
                _backBuffer[index] = b;
                _backBuffer[index + 1] = g;
                _backBuffer[index + 2] = r;
                _backBuffer[index + 3] = a;
            }

            // Clearing Depth Buffer
            for (var index = 0; index < _depthBuffer.Length; index++)
            {
                _depthBuffer[index] = float.MaxValue;
            }
        }

        // Once everything is ready, we can flush the back buffer
        // into the front buffer. 
        public void SwapBuffers()
        {
            using (var stream = _bmp.PixelBuffer.AsStream())
            {
                // writing our byte[] back buffer into our WriteableBitmap stream
                stream.Write(_backBuffer, 0, _backBuffer.Length);
            }
            // request a redraw of the entire bitmap
            _bmp.Invalidate();
        }

        // Called to put a pixel on screen at a specific X,Y coordinates
        public void PutPixel(int x, int y, float z, Color color)
        {
            // As we have a 1-D Array for our back buffer
            // we need to know the equivalent cell in 1-D based
            // on the 2D coordinates on screen
            var index = (x + y * _renderWidth);
            var index4 = index * 4;

            // Protecting our buffer against threads concurrencies
            lock (_lockBuffer[index])
            {
                if (_depthBuffer[index] < z)
                {
                    return; // Discard
                }

                _depthBuffer[index] = z;

                _backBuffer[index4] = (byte)(color.B * 255);
                _backBuffer[index4 + 1] = (byte)(color.G * 255);
                _backBuffer[index4 + 2] = (byte)(color.R * 255);
                _backBuffer[index4 + 3] = (byte)(color.A * 255);
            }
        }

        // Project takes some 3D coordinates and transform them
        // in 2D coordinates using the transformation matrix
        // It also transform the same coordinates and the norma to the vertex 
        // in the 3D world
        public Vertex ProjectPoint(Vertex vertex, Matrix4 transMat, Matrix4 world)
        {
            // transforming the coordinates
            var point2d = vertex.Coordinates * transMat;
            point2d = new Vector3(point2d.X / point2d.Z, point2d.Y / point2d.Z, point2d.Z);

            // transforming the coordinates & the normal to the vertex in the 3D world
            var point3dWorld = vertex.Coordinates * world;
            var normal3dWorld = vertex.Normal * world;

            // The transformed coordinates will be based on coordinate system
            // starting on the center of the screen. But drawing on screen normally starts
            // from top left. We then need to transform them again to have x:0, y:0 on top left.
            var x = point2d.X * _renderWidth + _renderWidth / 2.0f;
            var y = -point2d.Y * _renderHeight + _renderHeight / 2.0f;

            return new Vertex
            {
                Coordinates = new Vector3(x, y, point2d.Z),
                Normal = normal3dWorld,
                WorldCoordinates = point3dWorld
            };
        }

        // DrawPoint calls PutPixel but does the clipping operation before
        public void DrawPoint(Vector3 point, Color color)
        {
            // Clipping what's visible on screen
            if (point.X >= 0 && point.Y >= 0 && point.X < _renderWidth && point.Y < _renderHeight)
            {
                // Drawing a red point
                PutPixel((int)point.X, (int)point.Y, point.Z, color);
            }
        }

        #region Draw Line

        public void DrawLine(Vector2 point0, Vector2 point1, Color color)
        {
            var dist = (point1 - point0).Length;

            // If the distance between the 2 points is less than 2 pixels
            // We're exiting
            if (dist < 2)
                return;

            // Find the middle point between first & second point
            var middlePoint = point0 + (point1 - point0) / 2;

            // We draw this point on screen
            DrawPoint(middlePoint.ToVector3(), color);

            // Recursive algorithm launched between first & middle point
            // and between middle & second point
            DrawLine(point0, middlePoint, color);
            DrawLine(middlePoint, point1, color);
        }

        // Bresenham’s line algorithm
        public void DrawBline(Vector2 point0, Vector2 point1, Color color)
        {
            int x0 = (int)point0.X;
            int y0 = (int)point0.Y;
            int x1 = (int)point1.X;
            int y1 = (int)point1.Y;

            var dx = Math.Abs(x1 - x0);
            var dy = Math.Abs(y1 - y0);
            var sx = (x0 < x1) ? 1 : -1;
            var sy = (y0 < y1) ? 1 : -1;
            var err = dx - dy;

            while (true)
            {
                DrawPoint(new Vector3(x0, y0, 0), color);

                if ((x0 == x1) && (y0 == y1)) break;
                var e2 = 2 * err;
                if (e2 > -dy) { err -= dy; x0 += sx; }
                if (e2 < dx) { err += dx; y0 += sy; }
            }
        }

        #endregion

        #region DrawTriangle

        // Clamping values to keep them between 0 and 1
        float Clamp(float value, float min = 0, float max = 1)
        {
            return Math.Max(min, Math.Min(value, max));
        }

        // Interpolating the value between 2 vertices 
        // min is the starting point, max the ending point
        // and gradient the % between the 2 points
        float Interpolate(float min, float max, float gradient)
        {
            return min + (max - min) * Clamp(gradient);
        }

        // drawing line between 2 points from left to right
        // papb -> pcpd
        // pa, pb, pc, pd must then be sorted before
        void ProcessScanLine(ScanLineData data, Vertex va, Vertex vb, Vertex vc, Vertex vd, Color color)
        {
            Vector3 pa = va.Coordinates;
            Vector3 pb = vb.Coordinates;
            Vector3 pc = vc.Coordinates;
            Vector3 pd = vd.Coordinates;

            // Thanks to current Y, we can compute the gradient to compute others values like
            // the starting X (sx) and ending X (ex) to draw between
            // if pa.Y == pb.Y or pc.Y == pd.Y, gradient is forced to 1
            var gradient1 = pa.Y != pb.Y ? (data.currentY - pa.Y) / (pb.Y - pa.Y) : 1;
            var gradient2 = pc.Y != pd.Y ? (data.currentY - pc.Y) / (pd.Y - pc.Y) : 1;

            int sx = (int)Interpolate(pa.X, pb.X, gradient1);
            int ex = (int)Interpolate(pc.X, pd.X, gradient2);

            // starting Z & ending Z
            float z1 = Interpolate(pa.Z, pb.Z, gradient1);
            float z2 = Interpolate(pc.Z, pd.Z, gradient2);

            // Interpolating normals on Y
            var snl = Interpolate(data.ndotla, data.ndotlb, gradient1);
            var enl = Interpolate(data.ndotlc, data.ndotld, gradient2);

            // drawing a line from left (sx) to right (ex) 
            for (var x = sx; x < ex; x++)
            {
                float gradient = (x - sx) / (float)(ex - sx);

                var z = Interpolate(z1, z2, gradient);
                var ndotl = Interpolate(snl, enl, gradient);

                // changing the color value using the cosine of the angle
                // between the light vector and the normal vector
                DrawPoint(new Vector3(x, data.currentY, z), color * ndotl);
            }
        }

        // Compute the cosine of the angle between the light vector and the normal vector
        // Returns a value between 0 and 1
        private float ComputeNDotL(Vector3 vertex, Vector3 normal, Vector3 lightPosition)
        {
            var lightDirection = lightPosition - vertex;

            normal = normal.Normalize();
            lightDirection = lightDirection.Normalize();

            return Math.Max(0, Vector3.DotProduct(normal, lightDirection));
        }

        public void DrawTriangle(Vertex v1, Vertex v2, Vertex v3, Color color)
        {
            // Sorting the points in order to always have this order on screen p1, p2 & p3
            // with p1 always up (thus having the Y the lowest possible to be near the top screen)
            // then p2 between p1 & p3
            if (v1.Coordinates.Y > v2.Coordinates.Y)
            {
                var temp = v2;
                v2 = v1;
                v1 = temp;
            }

            if (v2.Coordinates.Y > v3.Coordinates.Y)
            {
                var temp = v2;
                v2 = v3;
                v3 = temp;
            }

            if (v1.Coordinates.Y > v2.Coordinates.Y)
            {
                var temp = v2;
                v2 = v1;
                v1 = temp;
            }

            Vector3 p1 = v1.Coordinates;
            Vector3 p2 = v2.Coordinates;
            Vector3 p3 = v3.Coordinates;

            // Light position 
            Vector3 lightPos = new Vector3(0, 10, -10);
            // computing the cos of the angle between the light vector and the normal vector
            // it will return a value between 0 and 1 that will be used as the intensity of the color
            float nl1 = ComputeNDotL(v1.WorldCoordinates, v1.Normal, lightPos);
            float nl2 = ComputeNDotL(v2.WorldCoordinates, v2.Normal, lightPos);
            float nl3 = ComputeNDotL(v3.WorldCoordinates, v3.Normal, lightPos);

            var data = new ScanLineData {  };

            // inverse slopes
            float dP1P2, dP1P3;

            // http://en.wikipedia.org/wiki/Slope
            // Computing inverse slopes
            if (p2.Y - p1.Y > 0)
                dP1P2 = (p2.X - p1.X) / (p2.Y - p1.Y);
            else
                dP1P2 = 0;

            if (p3.Y - p1.Y > 0)
                dP1P3 = (p3.X - p1.X) / (p3.Y - p1.Y);
            else
                dP1P3 = 0;

            // First case where triangles are like that:
            // P1
            // -
            // -- 
            // - -
            // -  -
            // -   - P2
            // -  -
            // - -
            // -
            // P3
            if (dP1P2 > dP1P3)
            {
                for (var y = (int)p1.Y; y <= (int)p3.Y; y++)
                {
                    data.currentY = y;

                    if (y < p2.Y)
                    {
                        data.ndotla = nl1;
                        data.ndotlb = nl3;
                        data.ndotlc = nl1;
                        data.ndotld = nl2;

                        ProcessScanLine(data, v1, v3, v1, v2, color);
                    }
                    else
                    {
                        data.ndotla = nl1;
                        data.ndotlb = nl3;
                        data.ndotlc = nl2;
                        data.ndotld = nl3;

                        ProcessScanLine(data, v1, v3, v2, v3, color);
                    }
                }

                //Parallel.For((int)p1.Y, (int)p3.Y + 1, y =>
                //   {
                //       if (y < p2.Y)
                //       {
                //           ProcessScanLine(y, p1, p3, p1, p2, color);
                //       }
                //       else
                //       {
                //           ProcessScanLine(y, p1, p3, p2, p3, color);
                //       }
                //   });
            }
            // First case where triangles are like that:
            //       P1
            //        -
            //       -- 
            //      - -
            //     -  -
            // P2 -   - 
            //     -  -
            //      - -
            //        -
            //       P3
            else
            {
                for (var y = (int)p1.Y; y <= (int)p3.Y; y++)
                {
                    data.currentY = y;

                    if (y < p2.Y)
                    {
                        data.ndotla = nl1;
                        data.ndotlb = nl2;
                        data.ndotlc = nl1;
                        data.ndotld = nl3;

                        ProcessScanLine(data, v1, v2, v1, v3, color);
                    }
                    else
                    {
                        data.ndotla = nl2;
                        data.ndotlb = nl3;
                        data.ndotlc = nl1;
                        data.ndotld = nl3;

                        ProcessScanLine(data, v2, v3, v1, v3, color);
                    }
                }

                //Parallel.For((int)p1.Y, (int)p3.Y + 1, y =>
                //  {
                //      if (y < p2.Y)
                //      {
                //          ProcessScanLine(y, p1, p2, p1, p3, color);
                //      }
                //      else
                //      {
                //          ProcessScanLine(y, p2, p3, p1, p3, color);
                //      }
                //  });
            }
        }

        #endregion

        // The main method of the engine that re-compute each vertex projection
        // during each frame
        public void Render(Camera camera, params Mesh[] meshes)
        {
            var viewMatrix = camera.GetViewMatrixLH();
            // fov - 0.78f
            var projectionMatrix = ProjectionUtilities.GetPerspectiveMatrixRH(0.78f, (float)_renderWidth / _renderHeight, 0.01f, 1.0f);

            foreach (var mesh in meshes)
            {
                var rotationMatrix = TransformationUtilities.GetRotationMatrix(mesh.Rotation);
                var scaleMatrix = TransformationUtilities.GetScaleMatrix(mesh.Scale);
                var translationMatrix = TransformationUtilities.GetTranslationMatrix(mesh.Position);

                // Beware to apply rotation before translation 
                var worldMatrix = rotationMatrix * scaleMatrix * translationMatrix;

                var transformMatrix = worldMatrix * viewMatrix * projectionMatrix;

                #region Draw points

                //foreach (var vertex in mesh.Vertices)
                //{
                //    var point = ProjectPoint(vertex, transformMatrix);
                //    // Then we can draw on screen
                //    DrawPoint(point, Color.Red);
                //}

                #endregion

                #region Draw lines

                //foreach (var face in mesh.Faces)
                //{
                //    var vertexA = mesh.Vertices[face.A];
                //    var vertexB = mesh.Vertices[face.B];
                //    var vertexC = mesh.Vertices[face.C];

                //    var pixelA = ProjectPoint(vertexA, transformMatrix);
                //    var pixelB = ProjectPoint(vertexB, transformMatrix);
                //    var pixelC = ProjectPoint(vertexC, transformMatrix);

                //    Vector2 v1 = new Vector2(pixelA.X, pixelA.Y);
                //    Vector2 v2 = new Vector2(pixelB.X, pixelB.Y);
                //    Vector2 v3 = new Vector2(pixelC.X, pixelC.Y);

                //    DrawBline(v1, v2, Color.Red);
                //    DrawBline(v2, v3, Color.Red);
                //    DrawBline(v3, v1, Color.Red);
                //}

                #endregion

                #region Draw triangles

                //var faceIndex = 0;
                //foreach (var face in mesh.Faces)
                //{
                //    var vertexA = mesh.Vertices[face.A];
                //    var vertexB = mesh.Vertices[face.B];
                //    var vertexC = mesh.Vertices[face.C];

                //    var pixelA = ProjectPoint(vertexA, transformMatrix);
                //    var pixelB = ProjectPoint(vertexB, transformMatrix);
                //    var pixelC = ProjectPoint(vertexC, transformMatrix);

                //    var color = 0.25f + (faceIndex % mesh.Faces.Length) * 0.75f / mesh.Faces.Length;
                //    DrawTriangle(pixelA, pixelB, pixelC, new Color(color, color, color, 1));
                //    faceIndex++;
                //}

                Parallel.For(0, mesh.Faces.Length, faceIndex =>
                {
                    var face = mesh.Faces[faceIndex];
                    var vertexA = mesh.Vertices[face.A];
                    var vertexB = mesh.Vertices[face.B];
                    var vertexC = mesh.Vertices[face.C];

                    var pixelA = ProjectPoint(vertexA, transformMatrix, worldMatrix);
                    var pixelB = ProjectPoint(vertexB, transformMatrix, worldMatrix);
                    var pixelC = ProjectPoint(vertexC, transformMatrix, worldMatrix);

                    //var color = 0.25f + (faceIndex % mesh.Faces.Length) * 0.75f / mesh.Faces.Length;
                    var color = 1.0f;
                    DrawTriangle(pixelA, pixelB, pixelC, new Color(color, color, color, 1));
                    faceIndex++;
                });

                #endregion
            }
        }
    }
}