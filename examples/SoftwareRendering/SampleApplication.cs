﻿using ScratchEngine.Content;
using ScratchEngine.Math;
using ScratchEngine.Rendering;
using System.IO;
using System.Threading.Tasks;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using System;

namespace SoftwareRendering
{
    public class SampleApplication
    {
        private int _fps = 1;
        private int _frames = 0;
        private long _startTime = 0;

        private Device _device;
        private Mesh _mesh;
        private Mesh[] _meshes;

        private Camera _camera = new Camera();

        public SampleApplication(Image frontBufferImageControl)
        {
            // Choose the back buffer resolution here
            var bmp = new WriteableBitmap(640, 480);

            _device = new Device(bmp);

            // Our Image XAML control
            frontBufferImageControl.Source = bmp;

            #region cube mesh

            //_mesh = new Mesh("Cube", 8, 12);

            //_mesh.Vertices[0] = new Vector3(-1, 1, 1);
            //_mesh.Vertices[1] = new Vector3(1, 1, 1);
            //_mesh.Vertices[2] = new Vector3(-1, -1, 1);
            //_mesh.Vertices[3] = new Vector3(1, -1, 1);
            //_mesh.Vertices[4] = new Vector3(-1, 1, -1);
            //_mesh.Vertices[5] = new Vector3(1, 1, -1);
            //_mesh.Vertices[6] = new Vector3(1, -1, -1);
            //_mesh.Vertices[7] = new Vector3(-1, -1, -1);

            //_mesh.Faces[0] = new Face(a: 0, b: 1, c: 2);
            //_mesh.Faces[1] = new Face(a: 1, b: 2, c: 3);
            //_mesh.Faces[2] = new Face(a: 1, b: 3, c: 6);
            //_mesh.Faces[3] = new Face(a: 1, b: 5, c: 6);
            //_mesh.Faces[4] = new Face(a: 0, b: 1, c: 4);
            //_mesh.Faces[5] = new Face(a: 1, b: 4, c: 5);

            //_mesh.Faces[6] = new Face(a: 2, b: 3, c: 7);
            //_mesh.Faces[7] = new Face(a: 3, b: 6, c: 7);
            //_mesh.Faces[8] = new Face(a: 0, b: 2, c: 7);
            //_mesh.Faces[9] = new Face(a: 0, b: 4, c: 7);
            //_mesh.Faces[10] = new Face(a: 4, b: 5, c: 6);
            //_mesh.Faces[11] = new Face(a: 4, b: 6, c: 7);

            #endregion

            //var data = GetStream("chooper.babylon").Result;
            //_meshes = BabylonContent.LoadJSONFile(data);
            //foreach (var mesh in _meshes)
            //{
            //    mesh.Position = new Vector3(mesh.Position.X - 3, mesh.Position.Y - 8.5f, mesh.Position.Z);
            //    mesh.Scale = new Vector3(0.5f, 0.5f, 0.5f);
            //    mesh.Rotation = new Vector3(mesh.Rotation.X, mesh.Rotation.Y, mesh.Rotation.Z + (180f).ToRadians());
            //}

            var data = GetStream("monkey.babylon").Result;
            _meshes = BabylonContent.LoadJSONFile(data);

            _camera.LookAt(new Vector3(0, 0, 10.0f), Vector3.Origin, -Vector3.BasisY);
        }

        private async Task<string> GetStream(string fileName)
        {
            // code that uses await goes here
            var file = await Windows.ApplicationModel.Package.Current.InstalledLocation.GetFileAsync(fileName);
            var data = await Windows.Storage.FileIO.ReadTextAsync(file);

            return data;
        }

        public void Update()
        {
            float delta = 0.01f;
            //float delta = 0f;

            foreach (var mesh in _meshes)
            {
                // rotating slightly the meshes during each frame rendered
                mesh.Rotation = new Vector3(mesh.Rotation.X, mesh.Rotation.Y + delta, mesh.Rotation.Z);
            }
        }

        public void Render()
        {
            _device.Clear(0, 0, 0, 255);

            // Doing the various matrix operations
            _device.Render(_camera, _meshes);

            // Flushing the back buffer into the front buffer
            _device.SwapBuffers();
        }

        private Vector2 _prev = new Vector2(0, 0);

        public void InputMouse(float x, float y)
        {
            //var direction = new Vector2(x, y);

            //float eps = 5;

            //float angle = (10f).ToRadians();
            //float xDelta = x - _prev.X;
            //float yDelta = y - _prev.Y;

            //if (xDelta > eps)
            //    _camera.RotateY(-angle);
            //if (xDelta < -eps)
            //    _camera.RotateY(angle);

            //if (yDelta > eps)
            //    _camera.RotateX(-angle);
            //if (yDelta < -eps)
            //    _camera.RotateX(angle);

            //_prev = direction;
        }

        public void InputKey(int key)
        {
            //float angle = (10f).ToRadians();
            //if (key == 38)
            //    _camera.RotateX(angle);
            //if (key == 40)
            //    _camera.RotateX(-angle);

            //if (key == 37)
            //    _camera.RotateY(angle);
            //if (key == 39)
            //    _camera.RotateY(-angle);

            //float moveAmoumt = 0.1f;
            ////w
            //if (key == 87)
            //    _camera.Move(_camera.ForwardDirection, moveAmoumt);
            ////s
            //if (key == 83)
            //    _camera.Move(_camera.ForwardDirection, -moveAmoumt);
            ////a
            //if (key == 65)
            //    _camera.Move(_camera.LeftDirection, -moveAmoumt);
            ////d
            //if (key == 68)
            //    _camera.Move(_camera.RightDirection, -moveAmoumt);
        }
    }
}